﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace modelFirst
{
    public partial class Form1 : Form
    {
        private ModelFirstContainer db = new ModelFirstContainer();
 

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            GetEmployees();
        }
        public void GetEmployees()
        {
            List<SEmployee> employeesList = db.SEmployees.ToList();
            dataGridView1.DataSource = employeesList;
        }


        public void DeleteEmployee(int ID)
        {
            Employee employee = db.Employees.FirstOrDefault(x => x.Id == ID);
            db.Employees.Remove(employee);

            db.SaveChanges();
    

        }
 
        private void Delete_Click(object sender, EventArgs e)
        {
            int ID = int.Parse(Id.Text);



            DeleteEmployee(ID);
            GetEmployees();
        }

        public void AddEmployee(Employee employee)
        {

            db.Employees.Add(employee);
            db.SaveChanges();

        }

        private void Add_Click(object sender, EventArgs e)
        {
            Employee employee = new Employee()
            {

                FirstName = FirstName.Text.ToString(),
                LastName = LastName.Text.ToString(),
                Dob = Dob.Text.ToString(),
                PositionId = int.Parse(Position.Text),
                Salary = Salary.Text.ToString(),
            };

            AddEmployee(employee);
            GetEmployees();
            
        }

        public void UpdateEmployee(Employee employee)
        {

            db.Employees.AddOrUpdate(employee);
            db.SaveChanges();

        }


        private void Update_Click(object sender, EventArgs e)
        {
            
            Employee employee = new Employee()
            {
                Id = int.Parse(Id.Text),
                FirstName = FirstName.Text.ToString(),
                LastName = LastName.Text.ToString(),
                Dob = Dob.Text.ToString(),
                PositionId = int.Parse(Position.Text),
                Salary = Salary.Text.ToString(),
            };

            UpdateEmployee(employee);
            
            GetEmployees();
        }
    }
}
