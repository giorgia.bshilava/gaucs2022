namespace PetShop_Final
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string username { get; set; }

        [Required]
        [StringLength(250)]
        public string password { get; set; }

        public int position_id { get; set; }

        public int access_level { get; set; }

        public virtual UserType UserType { get; set; }
    }
}
