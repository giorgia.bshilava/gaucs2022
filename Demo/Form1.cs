﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace Demo
{
    public partial class Form1 : Form
    {
        SqlConnection con;
        SqlCommand cmd;

        DataSet ds = new DataSet("info");
        DataTable dt = new DataTable("DemoTable");

     

        public Form1()
        {
            InitializeComponent();
        


            con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
            cmd = new SqlCommand();

            cmd.Connection = con;
            ConCheck();



            DataColumn ID = new DataColumn();
            ID.DataType = System.Type.GetType("System.String");
            ID.ColumnName = "ID";
            ID.MaxLength = 500;

            DataColumn FName = new DataColumn();
            FName.DataType = System.Type.GetType("System.String");
            FName.ColumnName = "FirstName";
            FName.MaxLength = 50;

            DataColumn LName = new DataColumn();
            LName.DataType = System.Type.GetType("System.String");
            LName.ColumnName = "LastName";
            LName.MaxLength = 50;

            DataColumn Age = new DataColumn();
            Age.DataType = System.Type.GetType("System.Int32");
            Age.ColumnName = "Age";

            dt.Columns.Add(ID);
            dt.Columns.Add(FName);
            dt.Columns.Add(LName);
            dt.Columns.Add(Age);

        }

        private void ConCheck()
        {

            try
            {
                con.Open();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }
        public void ShowData()
        {
            con.Open();
  
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select ID, FirstName, LastName, Age from DemoTable";
            cmd.ExecuteNonQuery();


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            dataGridView1.DataSource = dt;
            con.Close();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ConCheck();
            ShowData();
        }

        private void AddEdit_Click(object sender, EventArgs e)
        {

            DataRow dr = dt.NewRow();
            dr[0] = ID.Text;
            dr[1] = FNameBox.Text;
            dr[2] = LNameBox.Text;
            dr[3] = AgeBox.Text;
            dt.Rows.Add(dr);
            ds.Tables.Add(dt);

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM DemoTable", con);
 

            SqlCommandBuilder builder = new SqlCommandBuilder(da);

            

           
            da.Update(ds, "DemoTable");

            builder.GetUpdateCommand();

            ds.Tables.Clear();

            ShowData();
            
            con.Close();
        }

         
    }
}
