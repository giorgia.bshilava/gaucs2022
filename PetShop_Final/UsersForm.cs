﻿using DevOne.Security.Cryptography.BCrypt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace PetShop_Final
{
    public partial class UsersForm : Form
    {
        private StockForm stockForm;
        private SoldForm soldForm;


        public UsersForm()
        {
            InitializeComponent();

            GetUserTypes();
            GetUsers();
        }
        void GetUsers()
        {


            using (var context = new Context())
            {
                var data = context.Users.Join(context.UserTypes, user => user.position_id,
                  user_type => user_type.id, (user, user_type) => new
                  {

                      Id = user.id,
                      Username = user.username,
                      Password = user.password,
                      Position = user_type.position_name,
                      Access_Level = user.access_level,



                  });
                StockGridView.DataSource = data.ToList();

            }





        }

        void GetUserTypes()
        {
            using (var context = new Context())
            {
                var data = context.UserTypes.ToList();
                var bindingSource = new BindingSource();
                bindingSource.DataSource = data;
                PositionBox.DataSource = bindingSource;
                PositionBox.DisplayMember = "position_name";
                PositionBox.ValueMember = "id";

            }
        }



        private void Add_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 2)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(UsernameBox.Text) || string.IsNullOrEmpty(PasswordBox.Text) || string.IsNullOrEmpty(AccessLevelBox.Text) || string.IsNullOrEmpty(AccessLevelBox.Text))
                {
                    MessageBox.Show("Enter all data!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        User user = new User()
                        {
                            username = UsernameBox.Text.ToString(),
                            password = BCrypt.Net.BCrypt.HashPassword(PasswordBox.Text),
                            position_id = int.Parse(PositionBox.SelectedValue.ToString()),
                            access_level = int.Parse(AccessLevelBox.Text.ToString()),

                        };

                        context.Users.Add(user);
                        context.SaveChanges();
                        GetUsers();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect data format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {

            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text) || string.IsNullOrEmpty(UsernameBox.Text) || string.IsNullOrEmpty(PasswordBox.Text) || string.IsNullOrEmpty(AccessLevelBox.Text) || string.IsNullOrEmpty(AccessLevelBox.Text))
                {
                    MessageBox.Show("Enter all data!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        User user = new User()
                        {
                            id = int.Parse(IdBox.Text),
                            username = UsernameBox.Text.ToString(),
                            password = BCrypt.Net.BCrypt.HashPassword(PasswordBox.Text),
                            position_id = int.Parse(PositionBox.SelectedValue.ToString()),
                            access_level = int.Parse(AccessLevelBox.Text.ToString()),

                        };

                        context.Users.AddOrUpdate(user);
                        context.SaveChanges();
                        GetUsers();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect data format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text))
                {
                    MessageBox.Show("Enter id of the product to delete!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        int id = int.Parse(IdBox.Text);
                        DialogResult res = MessageBox.Show($"Are you sure you want to delete product number {id}?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (res == DialogResult.OK)
                        {
                            User user = context.Users.FirstOrDefault(x => x.id == id);
                            context.Users.Remove(user);
                            context.SaveChanges();
                            GetUsers();


                        }


                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect id format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }



        private void SelectBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            stockForm = new StockForm();
            soldForm = new SoldForm();
            Form formToShow = null;
            switch (SelectBox.SelectedItem.ToString())
            {

                case "Stock":
                    formToShow = stockForm;
                    break;
                case "Sold":
                    formToShow = soldForm;
                    break;
            }

            if (formToShow != null)
            {
                formToShow.Show();
                formToShow.BringToFront();
                formToShow.Closed += (s, args) => this.Close();
                this.Hide();
            }

        }

        private void Find_Click(object sender, EventArgs e)
        {



            var context = new Context();
            var name = new SqlParameter("@name", SearchBox.Text.ToString());
            var data = context.Users
                                   .SqlQuery("EXEC SearchUsersByUsername @name", name)
                                   .Join(context.UserTypes, user => user.position_id,
                  user_type => user_type.id, (user, user_type) => new
                  {


                      Id = user.id,
                      Username = user.username,
                      Password = user.password,
                      Position = user_type.position_name,
                      Access_Level = user.access_level,


                  });
            StockGridView.DataSource = data.ToList();


        }
        private void StockGridView_DoubleClick(object sender, EventArgs e)
        {

            if (StockGridView.SelectedRows.Count == 1)
            {
                IdBox.Text = StockGridView.SelectedRows[0].Cells["id"].Value.ToString();
                UsernameBox.Text = StockGridView.SelectedRows[0].Cells["username"].Value.ToString();
                PasswordBox.Text = StockGridView.SelectedRows[0].Cells["password"].Value.ToString();
                PositionBox.Text = StockGridView.SelectedRows[0].Cells["position"].Value.ToString();
                AccessLevelBox.Text = StockGridView.SelectedRows[0].Cells["access_level"].Value.ToString();

            }
        }


    }
}