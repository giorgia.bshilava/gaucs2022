﻿namespace Linq
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ShowProducts = new System.Windows.Forms.Button();
            this.ShowProductTypes = new System.Windows.Forms.Button();
            this.Grid1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
            this.SuspendLayout();
            // 
            // ShowProducts
            // 
            this.ShowProducts.Location = new System.Drawing.Point(101, 333);
            this.ShowProducts.Name = "ShowProducts";
            this.ShowProducts.Size = new System.Drawing.Size(119, 33);
            this.ShowProducts.TabIndex = 0;
            this.ShowProducts.Text = "Show Products";
            this.ShowProducts.UseVisualStyleBackColor = true;
            this.ShowProducts.Click += new System.EventHandler(this.ShowProducts_Click);
            // 
            // ShowProductTypes
            // 
            this.ShowProductTypes.Location = new System.Drawing.Point(595, 333);
            this.ShowProductTypes.Name = "ShowProductTypes";
            this.ShowProductTypes.Size = new System.Drawing.Size(119, 33);
            this.ShowProductTypes.TabIndex = 1;
            this.ShowProductTypes.Text = "Show Product Types";
            this.ShowProductTypes.UseVisualStyleBackColor = true;
            // 
            // Grid1
            // 
            this.Grid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grid1.Location = new System.Drawing.Point(101, 29);
            this.Grid1.Name = "Grid1";
            this.Grid1.Size = new System.Drawing.Size(613, 175);
            this.Grid1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Grid1);
            this.Controls.Add(this.ShowProductTypes);
            this.Controls.Add(this.ShowProducts);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ShowProducts;
        private System.Windows.Forms.Button ShowProductTypes;
        private System.Windows.Forms.DataGridView Grid1;
    }
}

