﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Linq
{
    public partial class Form1 : Form
    {
        SqlConnection con;
        SqlCommand cmd;

        private linqEntities db = new linqEntities();
        private object linq;

        
        public Form1()
        {
            InitializeComponent();

            
            cmd = new SqlCommand();

            cmd.Connection = con;
           
        }

        private void ShowProducts_Click(object sender, EventArgs e)
        {
            GetProducts();
        }
        public void GetProducts()
        {
            List<Product> productsList = db.Products.ToList();
            Grid1.DataSource = productsList;
        }
    }

     
}
