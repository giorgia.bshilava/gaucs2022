﻿using DevOne.Security.Cryptography.BCrypt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace PetShop_Final
{
    public partial class SoldForm : Form
    {
        private StockForm stockForm;
        private UsersForm usersForm;
        private ReportForm reportForm;


        public SoldForm()
        {
            InitializeComponent();
 
            GetSuppliers();
            GetSold();
        }
        void GetSold()
        {

            using (var context = new Context())
            {
                var data = context.Solds.Join(context.Suppliers, sold => sold.supplier_id,
                  supplier => supplier.id, (sold, supplier) => new
                  {

                      Id = sold.id,
                      Product_Name = sold.product_name,
                      Amount = sold.amount,
                      Price = sold.price,
                      Supplier = supplier.supplier_name,
                      Country = supplier.country


                  });
                StockGridView.DataSource = data.ToList();

            }




        }

        void GetSuppliers()
        {
            using (var context = new Context())
            {
                var data = context.Suppliers.ToList();
                var bindingSource = new BindingSource();
                bindingSource.DataSource = data;
                SupplierBox.DataSource = bindingSource;
                SupplierBox.DisplayMember = "supplier_name";
                SupplierBox.ValueMember = "id";
            
            }
        }



        private void Add_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(ProductNameBox.Text) || string.IsNullOrEmpty(AmountBox.Text) || string.IsNullOrEmpty(PriceBox.Text) || string.IsNullOrEmpty(SupplierBox.Text))
                {
                    MessageBox.Show("Enter all data!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        Sold sold = new Sold()
                        {
                            product_name = ProductNameBox.Text.ToString(),
                            amount = int.Parse(AmountBox.Text),
                            price = decimal.Parse(PriceBox.Text),
                            supplier_id = int.Parse(SupplierBox.SelectedValue.ToString()),

                        };

                        context.Solds.Add(sold);
                        context.SaveChanges();
                        GetSold();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect data format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {

            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text) || string.IsNullOrEmpty(ProductNameBox.Text) || string.IsNullOrEmpty(AmountBox.Text) || string.IsNullOrEmpty(PriceBox.Text) || string.IsNullOrEmpty(SupplierBox.Text))
                {
                    MessageBox.Show("Enter all data!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        Sold sold = new Sold()
                        {
                            id = int.Parse(IdBox.Text),
                            product_name = ProductNameBox.Text.ToString(),
                            amount = int.Parse(AmountBox.Text),
                            price = decimal.Parse(PriceBox.Text),
                            supplier_id = int.Parse(SupplierBox.SelectedValue.ToString()),

                        };

                        context.Solds.AddOrUpdate(sold);
                        context.SaveChanges();
                        GetSold();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect data format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text))
                {
                    MessageBox.Show("Enter id of the product to delete!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        int id = int.Parse(IdBox.Text);
                        DialogResult res = MessageBox.Show($"Are you sure you want to delete product number {id}?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (res == DialogResult.OK)
                        {
                            Sold sold = context.Solds.FirstOrDefault(x => x.id == id);
                            context.Solds.Remove(sold);
                            context.SaveChanges();
                            GetSold();


                        }


                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect id format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Sell_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text))
                {
                    MessageBox.Show("Enter id of the product to return to stock!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        int id = int.Parse(IdBox.Text);
                        DialogResult res = MessageBox.Show($"Are you sure you want to return to stock product number {id}?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (res == DialogResult.OK)
                        {
                            var sold = context.Solds.Find(id);
                            var transaction = context.Database.BeginTransaction();
                            Stock stock = new Stock();
                            stock.id = sold.id;
                            stock.product_name = sold.product_name;
                            stock.amount = sold.amount;
                            stock.price = sold.price;
                            stock.supplier_id = sold.supplier_id;


                            context.Solds.Remove(sold);
                            context.Stocks.Add(stock);
                            context.SaveChanges();
                            transaction.Commit();
                            GetSold();


                        }


                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect id format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void SelectBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            usersForm = new UsersForm();
            stockForm = new StockForm();
            Form formToShow = null;
            switch (SelectBox.SelectedItem.ToString())
            {

                case "Stock":
                    formToShow = stockForm;
                    break;
                case "Users":
                    formToShow = usersForm;
                    break;
            }

            if (formToShow != null)
            {
                if (Properties.Settings.Default.User < 2 && formToShow == usersForm)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    formToShow = null;
                }
                else
                {

                    formToShow.Show();
                    formToShow.BringToFront();
                    formToShow.Closed += (s, args) => this.Close();
                    this.Hide();
                }
            }

        }

        private void Find_Click(object sender, EventArgs e)
        {



            var context = new Context();
            var name = new SqlParameter("@name", SearchBox.Text.ToString());
            var data = context.Solds
                                   .SqlQuery("EXEC SearchSoldByName @name", name)
                                   .Join(context.Suppliers, stock => stock.supplier_id,
                  supplier => supplier.id, (stock, supplier) => new
                  {

                      Id = stock.id,
                      Product_Name = stock.product_name,
                      Amount = stock.amount,
                      Price = stock.price,
                      Supplier = supplier.supplier_name,
                      Country = supplier.country


                  });
            StockGridView.DataSource = data.ToList();

 
        }
        private void StockGridView_DoubleClick(object sender, EventArgs e)
        {
            
            if (StockGridView.SelectedRows.Count == 1)
            {
                IdBox.Text = StockGridView.SelectedRows[0].Cells["id"].Value.ToString();
                ProductNameBox.Text = StockGridView.SelectedRows[0].Cells["product_name"].Value.ToString();
                AmountBox.Text = StockGridView.SelectedRows[0].Cells["amount"].Value.ToString();
                PriceBox.Text = StockGridView.SelectedRows[0].Cells["price"].Value.ToString();
                SupplierBox.Text = StockGridView.SelectedRows[0].Cells["supplier"].Value.ToString();

            }
        }

        private void ShowReport_Click(object sender, EventArgs e)
        {
            var reportForm = new ReportForm();
            reportForm.Show();
            reportForm.BringToFront();
        
          
        }
    }
}