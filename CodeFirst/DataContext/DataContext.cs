﻿//using CodeFirst.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst 
{
    internal class DataContext : DbContext
    {
        public DataContext(string connectionString) : base(connectionString)
        {
        }

        //public DataContext() : base("data source=DESKTOP-L5NMGLP;initial catalog=CodeFirst;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;\" providerName=\"System.Data.EntityClient\"")
        //{
        //}


        public virtual DbSet<User> Users { get; set; }

 
    }
}
