﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevOne.Security.Cryptography.BCrypt;
 

namespace PetShop_Final
{
    public partial class authorisation : Form
    {
        public authorisation()
        {
            InitializeComponent();
        }
       
     
        private void LogIn_Click(object sender, EventArgs e)
        {

            using (var context = new Context())
            {
                var user = context.Users.FirstOrDefault(u => u.username == UsernameBox.Text);
                if (user != null)
                {
                    bool isCorrect = BCryptHelper.CheckPassword(PasswordBox.Text, user.password);
                    if (isCorrect)
                    {

                        Properties.Settings.Default.User =  user.access_level;

                        this.Hide();
                        StockForm frm1 = new StockForm();
                        frm1.Closed += (s, args) => this.Close();
                        frm1.Show();

                    }
                    else
                    {
                        MessageBox.Show("Incorrect password!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    }
                }
                else
                {
                    MessageBox.Show("This username does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void authorisation_Load(object sender, EventArgs e)
        {
            UsernameBox.Text = "abshilava";
            PasswordBox.Text = "12345";
        }
    }
}
