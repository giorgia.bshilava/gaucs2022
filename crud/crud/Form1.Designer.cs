﻿namespace Linq
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ShowProducts = new System.Windows.Forms.Button();
            this.ShowProductTypes = new System.Windows.Forms.Button();
            this.Grid1 = new System.Windows.Forms.DataGridView();
            this.ProductNameBox = new System.Windows.Forms.TextBox();
            this.ProductTypeBox = new System.Windows.Forms.TextBox();
            this.ManufacturerBox = new System.Windows.Forms.TextBox();
            this.PriceBox = new System.Windows.Forms.TextBox();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.Product_IDBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
            this.SuspendLayout();
            // 
            // ShowProducts
            // 
            this.ShowProducts.Location = new System.Drawing.Point(101, 248);
            this.ShowProducts.Name = "ShowProducts";
            this.ShowProducts.Size = new System.Drawing.Size(119, 33);
            this.ShowProducts.TabIndex = 0;
            this.ShowProducts.Text = "Show Products";
            this.ShowProducts.UseVisualStyleBackColor = true;
            this.ShowProducts.Click += new System.EventHandler(this.ShowProducts_Click);
            // 
            // ShowProductTypes
            // 
            this.ShowProductTypes.Location = new System.Drawing.Point(595, 248);
            this.ShowProductTypes.Name = "ShowProductTypes";
            this.ShowProductTypes.Size = new System.Drawing.Size(119, 33);
            this.ShowProductTypes.TabIndex = 1;
            this.ShowProductTypes.Text = "Show Product Types";
            this.ShowProductTypes.UseVisualStyleBackColor = true;
            this.ShowProductTypes.Click += new System.EventHandler(this.ShowProductTypes_Click);
            // 
            // Grid1
            // 
            this.Grid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grid1.Location = new System.Drawing.Point(101, 29);
            this.Grid1.Name = "Grid1";
            this.Grid1.Size = new System.Drawing.Size(613, 175);
            this.Grid1.TabIndex = 2;
            // 
            // ProductNameBox
            // 
            this.ProductNameBox.Location = new System.Drawing.Point(286, 279);
            this.ProductNameBox.Name = "ProductNameBox";
            this.ProductNameBox.Size = new System.Drawing.Size(257, 20);
            this.ProductNameBox.TabIndex = 3;
            this.ProductNameBox.Text = "Product Name";
            // 
            // ProductTypeBox
            // 
            this.ProductTypeBox.Location = new System.Drawing.Point(286, 357);
            this.ProductTypeBox.Name = "ProductTypeBox";
            this.ProductTypeBox.Size = new System.Drawing.Size(257, 20);
            this.ProductTypeBox.TabIndex = 4;
            this.ProductTypeBox.Text = "Product Type";
            // 
            // ManufacturerBox
            // 
            this.ManufacturerBox.Location = new System.Drawing.Point(286, 305);
            this.ManufacturerBox.Name = "ManufacturerBox";
            this.ManufacturerBox.Size = new System.Drawing.Size(257, 20);
            this.ManufacturerBox.TabIndex = 5;
            this.ManufacturerBox.Text = "Manufacturer";
            // 
            // PriceBox
            // 
            this.PriceBox.Location = new System.Drawing.Point(286, 331);
            this.PriceBox.Name = "PriceBox";
            this.PriceBox.Size = new System.Drawing.Size(257, 20);
            this.PriceBox.TabIndex = 6;
            this.PriceBox.Text = "Price";
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(468, 395);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 7;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(378, 395);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(75, 23);
            this.UpdateButton.TabIndex = 8;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(286, 395);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 9;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // Product_IDBox
            // 
            this.Product_IDBox.Location = new System.Drawing.Point(286, 248);
            this.Product_IDBox.Name = "Product_IDBox";
            this.Product_IDBox.Size = new System.Drawing.Size(257, 20);
            this.Product_IDBox.TabIndex = 10;
            this.Product_IDBox.Text = "Product ID";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Product_IDBox);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.PriceBox);
            this.Controls.Add(this.ManufacturerBox);
            this.Controls.Add(this.ProductTypeBox);
            this.Controls.Add(this.ProductNameBox);
            this.Controls.Add(this.Grid1);
            this.Controls.Add(this.ShowProductTypes);
            this.Controls.Add(this.ShowProducts);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ShowProducts;
        private System.Windows.Forms.Button ShowProductTypes;
        private System.Windows.Forms.DataGridView Grid1;
        private System.Windows.Forms.TextBox ProductNameBox;
        private System.Windows.Forms.TextBox ProductTypeBox;
        private System.Windows.Forms.TextBox ManufacturerBox;
        private System.Windows.Forms.TextBox PriceBox;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.TextBox Product_IDBox;
    }
}

