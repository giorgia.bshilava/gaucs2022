﻿using DevOne.Security.Cryptography.BCrypt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace PetShop_Final
{
    public partial class StockForm : Form
    {
        private SoldForm soldForm;
        private UsersForm usersForm;


        public StockForm()
        {
            InitializeComponent();
         
            GetSuppliers();
            GetStock();
        }
        void GetStock()
        {

            using (var context = new Context())
            {
                var data = context.Stocks.Join(context.Suppliers, stock => stock.supplier_id,
                  supplier => supplier.id, (stock, supplier) => new
                  {

                      Id = stock.id,
                      Product_Name = stock.product_name,
                      Amount = stock.amount,
                      Price = stock.price,
                      Supplier = supplier.supplier_name,
                      Country = supplier.country


                  });
                StockGridView.DataSource = data.ToList();

            }




        }

        void GetSuppliers()
        {
            using (var context = new Context())
            {
                var data = context.Suppliers.ToList();
                var bindingSource = new BindingSource();
                bindingSource.DataSource = data;
                SupplierBox.DataSource = bindingSource;
                SupplierBox.DisplayMember = "supplier_name";
                SupplierBox.ValueMember = "id";
            
            }
        }



        private void Add_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(ProductNameBox.Text) || string.IsNullOrEmpty(AmountBox.Text) || string.IsNullOrEmpty(PriceBox.Text) || string.IsNullOrEmpty(SupplierBox.Text))
                {
                    MessageBox.Show("Enter all data!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        Stock stock = new Stock()
                        {
                            product_name = ProductNameBox.Text.ToString(),
                            amount = int.Parse(AmountBox.Text),
                            price = decimal.Parse(PriceBox.Text),
                            supplier_id = int.Parse(SupplierBox.SelectedValue.ToString()),

                        };

                        context.Stocks.Add(stock);
                        context.SaveChanges();
                        GetStock();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect data format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {

            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text) || string.IsNullOrEmpty(ProductNameBox.Text) || string.IsNullOrEmpty(AmountBox.Text) || string.IsNullOrEmpty(PriceBox.Text) || string.IsNullOrEmpty(SupplierBox.Text))
                {
                    MessageBox.Show("Enter all data!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        Stock stock = new Stock()
                        {
                            id = int.Parse(IdBox.Text),
                            product_name = ProductNameBox.Text.ToString(),
                            amount = int.Parse(AmountBox.Text),
                            price = decimal.Parse(PriceBox.Text),
                            supplier_id = int.Parse(SupplierBox.SelectedValue.ToString()),

                        };

                        context.Stocks.AddOrUpdate(stock);
                        context.SaveChanges();
                        GetStock();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect data format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text))
                {
                    MessageBox.Show("Enter id of the product to delete!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        int id = int.Parse(IdBox.Text);
                        DialogResult res = MessageBox.Show($"Are you sure you want to delete product number {id}?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (res == DialogResult.OK)
                        {
                            Stock stock = context.Stocks.FirstOrDefault(x => x.id == id);
                            context.Stocks.Remove(stock);
                            context.SaveChanges();
                            GetStock();


                        }


                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect id format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void Sell_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                if (Properties.Settings.Default.User < 1)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (string.IsNullOrEmpty(IdBox.Text))
                {
                    MessageBox.Show("Enter id of the product to sell!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {


                        int id = int.Parse(IdBox.Text);
                        DialogResult res = MessageBox.Show($"Are you sure you want to sell product number {id}?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (res == DialogResult.OK)
                        {
                            Stock stock = context.Stocks.Find(id);
                            var transaction = context.Database.BeginTransaction();
                            Sold sold = new Sold();
                            sold.id = stock.id;
                            sold.product_name = stock.product_name;
                            sold.amount = stock.amount;
                            sold.price = stock.price;
                            sold.supplier_id = stock.supplier_id;


                            context.Stocks.Remove(stock);
                            context.Solds.Add(sold);
                            context.SaveChanges();
                            transaction.Commit();
                            GetStock();


                        }


                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Incorrect id format! ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
        }

        private void SelectBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            usersForm = new UsersForm();
            soldForm = new SoldForm();
            Form formToShow = null;
            switch (SelectBox.SelectedItem.ToString())
            {

                case "Sold":
                    formToShow = soldForm;
                    break;
                case "Users":
                    formToShow = usersForm;
                    break;
            }

            if (formToShow != null)
            {
                if (Properties.Settings.Default.User < 2 && formToShow == usersForm)
                {
                    MessageBox.Show("Acces denied!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    formToShow = null;
                }
                else
                {

                    formToShow.Show();
                    formToShow.BringToFront();
                    formToShow.Closed += (s, args) => this.Close();
                    this.Hide();
                }
            }

        }

        private void Find_Click(object sender, EventArgs e)
        {



            var context = new Context();
            var name = new SqlParameter("@name", SearchBox.Text.ToString());
            var data = context.Stocks
                                   .SqlQuery("EXEC SearchStockByName @name", name)
                                   .Join(context.Suppliers, stock => stock.supplier_id,
                  supplier => supplier.id, (stock, supplier) => new
                  {

                      Id = stock.id,
                      Product_Name = stock.product_name,
                      Amount = stock.amount,
                      Price = stock.price,
                      Supplier = supplier.supplier_name,
                      Country = supplier.country


                  });
            StockGridView.DataSource = data.ToList();

 
        }
        private void StockGridView_DoubleClick(object sender, EventArgs e)
        {
            
            if (StockGridView.SelectedRows.Count == 1)
            {
                IdBox.Text = StockGridView.SelectedRows[0].Cells["id"].Value.ToString();
                ProductNameBox.Text = StockGridView.SelectedRows[0].Cells["product_name"].Value.ToString();
                AmountBox.Text = StockGridView.SelectedRows[0].Cells["amount"].Value.ToString();
                PriceBox.Text = StockGridView.SelectedRows[0].Cells["price"].Value.ToString();
                SupplierBox.Text = StockGridView.SelectedRows[0].Cells["supplier"].Value.ToString();

            }
        }
    
       
    }
}