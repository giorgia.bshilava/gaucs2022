﻿namespace Demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.AgeBox = new System.Windows.Forms.TextBox();
            this.LNameBox = new System.Windows.Forms.TextBox();
            this.FNameBox = new System.Windows.Forms.TextBox();
            this.AddEdit = new System.Windows.Forms.Button();
            this.ID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(112, 21);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(559, 264);
            this.dataGridView1.TabIndex = 0;
            // 
            // AgeBox
            // 
            this.AgeBox.Location = new System.Drawing.Point(571, 322);
            this.AgeBox.Name = "AgeBox";
            this.AgeBox.Size = new System.Drawing.Size(100, 20);
            this.AgeBox.TabIndex = 1;
            this.AgeBox.Text = "Age";
            // 
            // LNameBox
            // 
            this.LNameBox.Location = new System.Drawing.Point(426, 322);
            this.LNameBox.Name = "LNameBox";
            this.LNameBox.Size = new System.Drawing.Size(100, 20);
            this.LNameBox.TabIndex = 2;
            this.LNameBox.Text = "Last Name";
            // 
            // FNameBox
            // 
            this.FNameBox.Location = new System.Drawing.Point(264, 322);
            this.FNameBox.Name = "FNameBox";
            this.FNameBox.Size = new System.Drawing.Size(100, 20);
            this.FNameBox.TabIndex = 3;
            this.FNameBox.Text = "First Name";
            // 
            // AddEdit
            // 
            this.AddEdit.Location = new System.Drawing.Point(348, 374);
            this.AddEdit.Name = "AddEdit";
            this.AddEdit.Size = new System.Drawing.Size(100, 43);
            this.AddEdit.TabIndex = 4;
            this.AddEdit.Text = "Add/Edit";
            this.AddEdit.UseVisualStyleBackColor = true;
            this.AddEdit.Click += new System.EventHandler(this.AddEdit_Click);
            // 
            // ID
            // 
            this.ID.Location = new System.Drawing.Point(112, 322);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(100, 20);
            this.ID.TabIndex = 5;
            this.ID.Text = "ID";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.AddEdit);
            this.Controls.Add(this.FNameBox);
            this.Controls.Add(this.LNameBox);
            this.Controls.Add(this.AgeBox);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox AgeBox;
        private System.Windows.Forms.TextBox LNameBox;
        private System.Windows.Forms.TextBox FNameBox;
        private System.Windows.Forms.Button AddEdit;
        private System.Windows.Forms.TextBox ID;
    }
}

