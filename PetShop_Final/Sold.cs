namespace PetShop_Final
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sold")]
    public partial class Sold
    {
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string product_name { get; set; }

        public int amount { get; set; }

        public decimal price { get; set; }

        public int supplier_id { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
