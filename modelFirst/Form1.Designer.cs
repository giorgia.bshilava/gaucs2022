﻿namespace modelFirst
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.TextBox();
            this.FirstName = new System.Windows.Forms.TextBox();
            this.LastName = new System.Windows.Forms.TextBox();
            this.Position = new System.Windows.Forms.TextBox();
            this.Salary = new System.Windows.Forms.TextBox();
            this.Dob = new System.Windows.Forms.DateTimePicker();
            this.Add = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(38, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(696, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.Location = new System.Drawing.Point(38, 190);
            this.Id.Name = "Id";
            this.Id.Size = new System.Drawing.Size(237, 20);
            this.Id.TabIndex = 1;
            this.Id.Text = "Id";
            // 
            // FirstName
            // 
            this.FirstName.Location = new System.Drawing.Point(38, 216);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(237, 20);
            this.FirstName.TabIndex = 1;
            this.FirstName.Text = "FirstName";
            // 
            // LastName
            // 
            this.LastName.Location = new System.Drawing.Point(38, 242);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(237, 20);
            this.LastName.TabIndex = 1;
            this.LastName.Text = "LastName";
            // 
            // Position
            // 
            this.Position.Location = new System.Drawing.Point(38, 294);
            this.Position.Name = "Position";
            this.Position.Size = new System.Drawing.Size(237, 20);
            this.Position.TabIndex = 1;
            this.Position.Text = "Position";
            // 
            // Salary
            // 
            this.Salary.Location = new System.Drawing.Point(38, 320);
            this.Salary.Name = "Salary";
            this.Salary.Size = new System.Drawing.Size(237, 20);
            this.Salary.TabIndex = 1;
            this.Salary.Text = "Salary";
            // 
            // Dob
            // 
            this.Dob.Location = new System.Drawing.Point(38, 268);
            this.Dob.Name = "Dob";
            this.Dob.Size = new System.Drawing.Size(237, 20);
            this.Dob.TabIndex = 2;
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(38, 346);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 3;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Update
            // 
            this.Update.Location = new System.Drawing.Point(119, 346);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(75, 23);
            this.Update.TabIndex = 4;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(200, 346);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 23);
            this.Delete.TabIndex = 5;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Update);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.Dob);
            this.Controls.Add(this.Salary);
            this.Controls.Add(this.Position);
            this.Controls.Add(this.LastName);
            this.Controls.Add(this.FirstName);
            this.Controls.Add(this.Id);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox Id;
        private System.Windows.Forms.TextBox FirstName;
        private System.Windows.Forms.TextBox LastName;
        private System.Windows.Forms.TextBox Position;
        private System.Windows.Forms.TextBox Salary;
        private System.Windows.Forms.DateTimePicker Dob;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.Button Delete;
    }
}

