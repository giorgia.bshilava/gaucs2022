﻿namespace SmartCam
{
    partial class SmartCam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.Upd = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Brand = new System.Windows.Forms.TextBox();
            this.Model = new System.Windows.Forms.TextBox();
            this.BodiesKits = new System.Windows.Forms.TextBox();
            this.SensorSize = new System.Windows.Forms.TextBox();
            this.Price = new System.Windows.Forms.TextBox();
            this.VideoResolution = new System.Windows.Forms.TextBox();
            this.StillImageResolution = new System.Windows.Forms.TextBox();
            this.ProductCode = new System.Windows.Forms.TextBox();
            this.FindBox = new System.Windows.Forms.TextBox();
            this.Find = new System.Windows.Forms.Button();
            this.BuiltInFlash = new System.Windows.Forms.CheckBox();
            this.DualCardSlot = new System.Windows.Forms.CheckBox();
            this.HeadphoneOut = new System.Windows.Forms.CheckBox();
            this.Sell = new System.Windows.Forms.Button();
            this.Branch = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BalanceGrid = new System.Windows.Forms.DataGridView();
            this.FromBox = new System.Windows.Forms.ComboBox();
            this.ToBox = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Transfer = new System.Windows.Forms.Button();
            this.AmountBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.StreetBox = new System.Windows.Forms.TextBox();
            this.CityBox = new System.Windows.Forms.TextBox();
            this.InsertBranch = new System.Windows.Forms.Button();
            this.Street = new System.Windows.Forms.Label();
            this.City = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BalanceGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridView1.Location = new System.Drawing.Point(255, 46);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1188, 394);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(18, 591);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 31);
            this.button1.TabIndex = 0;
            this.button1.Text = "Show Stock";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ShowAll_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(127, 591);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 31);
            this.button2.TabIndex = 2;
            this.button2.Text = "Show Sold";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.ShowSold_Click);
            // 
            // Add
            // 
            this.Add.BackColor = System.Drawing.Color.Green;
            this.Add.Location = new System.Drawing.Point(18, 467);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(103, 30);
            this.Add.TabIndex = 3;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = false;
            this.Add.Click += new System.EventHandler(this.Insert_Click);
            // 
            // Upd
            // 
            this.Upd.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.Upd.Location = new System.Drawing.Point(18, 503);
            this.Upd.Name = "Upd";
            this.Upd.Size = new System.Drawing.Size(103, 30);
            this.Upd.TabIndex = 4;
            this.Upd.Text = "Update";
            this.Upd.UseVisualStyleBackColor = false;
            this.Upd.Click += new System.EventHandler(this.Upd_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Red;
            this.button5.Location = new System.Drawing.Point(127, 467);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(110, 30);
            this.button5.TabIndex = 5;
            this.button5.Text = "Remove";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.Remove_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Product Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Branch";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Brand";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Model";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Bodies/Kits";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Sensor Size";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Price";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 289);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Still Image Resolution";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 398);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Headphone Out";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 316);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Video Resolution";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 373);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "Built-in Flash";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 347);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Dual Card Slot";
            // 
            // Brand
            // 
            this.Brand.Location = new System.Drawing.Point(127, 150);
            this.Brand.Name = "Brand";
            this.Brand.Size = new System.Drawing.Size(100, 20);
            this.Brand.TabIndex = 22;
            // 
            // Model
            // 
            this.Model.Location = new System.Drawing.Point(127, 179);
            this.Model.Name = "Model";
            this.Model.Size = new System.Drawing.Size(100, 20);
            this.Model.TabIndex = 23;
            // 
            // BodiesKits
            // 
            this.BodiesKits.Location = new System.Drawing.Point(127, 206);
            this.BodiesKits.Name = "BodiesKits";
            this.BodiesKits.Size = new System.Drawing.Size(100, 20);
            this.BodiesKits.TabIndex = 24;
            // 
            // SensorSize
            // 
            this.SensorSize.Location = new System.Drawing.Point(127, 234);
            this.SensorSize.Name = "SensorSize";
            this.SensorSize.Size = new System.Drawing.Size(100, 20);
            this.SensorSize.TabIndex = 25;
            // 
            // Price
            // 
            this.Price.Location = new System.Drawing.Point(127, 260);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(100, 20);
            this.Price.TabIndex = 26;
            // 
            // VideoResolution
            // 
            this.VideoResolution.Location = new System.Drawing.Point(127, 313);
            this.VideoResolution.Name = "VideoResolution";
            this.VideoResolution.Size = new System.Drawing.Size(100, 20);
            this.VideoResolution.TabIndex = 27;
            // 
            // StillImageResolution
            // 
            this.StillImageResolution.Location = new System.Drawing.Point(127, 286);
            this.StillImageResolution.Name = "StillImageResolution";
            this.StillImageResolution.Size = new System.Drawing.Size(100, 20);
            this.StillImageResolution.TabIndex = 28;
            // 
            // ProductCode
            // 
            this.ProductCode.Location = new System.Drawing.Point(127, 97);
            this.ProductCode.Name = "ProductCode";
            this.ProductCode.Size = new System.Drawing.Size(100, 20);
            this.ProductCode.TabIndex = 29;
            // 
            // FindBox
            // 
            this.FindBox.Location = new System.Drawing.Point(255, 12);
            this.FindBox.Name = "FindBox";
            this.FindBox.Size = new System.Drawing.Size(1126, 20);
            this.FindBox.TabIndex = 34;
            // 
            // Find
            // 
            this.Find.BackColor = System.Drawing.SystemColors.Highlight;
            this.Find.Location = new System.Drawing.Point(1387, 7);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(56, 28);
            this.Find.TabIndex = 35;
            this.Find.Text = "FIND";
            this.Find.UseVisualStyleBackColor = false;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // BuiltInFlash
            // 
            this.BuiltInFlash.AutoSize = true;
            this.BuiltInFlash.Location = new System.Drawing.Point(127, 372);
            this.BuiltInFlash.Name = "BuiltInFlash";
            this.BuiltInFlash.Size = new System.Drawing.Size(15, 14);
            this.BuiltInFlash.TabIndex = 36;
            this.BuiltInFlash.UseVisualStyleBackColor = true;
            // 
            // DualCardSlot
            // 
            this.DualCardSlot.AutoSize = true;
            this.DualCardSlot.Location = new System.Drawing.Point(127, 346);
            this.DualCardSlot.Name = "DualCardSlot";
            this.DualCardSlot.Size = new System.Drawing.Size(15, 14);
            this.DualCardSlot.TabIndex = 37;
            this.DualCardSlot.UseVisualStyleBackColor = true;
            // 
            // HeadphoneOut
            // 
            this.HeadphoneOut.AutoSize = true;
            this.HeadphoneOut.Location = new System.Drawing.Point(127, 398);
            this.HeadphoneOut.Name = "HeadphoneOut";
            this.HeadphoneOut.Size = new System.Drawing.Size(15, 14);
            this.HeadphoneOut.TabIndex = 38;
            this.HeadphoneOut.UseVisualStyleBackColor = true;
            // 
            // Sell
            // 
            this.Sell.BackColor = System.Drawing.Color.IndianRed;
            this.Sell.Location = new System.Drawing.Point(127, 503);
            this.Sell.Name = "Sell";
            this.Sell.Size = new System.Drawing.Size(110, 30);
            this.Sell.TabIndex = 39;
            this.Sell.Text = "Sell";
            this.Sell.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Sell.UseVisualStyleBackColor = false;
            this.Sell.Click += new System.EventHandler(this.Sell_Click);
            // 
            // Branch
            // 
            this.Branch.AllowDrop = true;
            this.Branch.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.Branch.DisplayMember = "True";
            this.Branch.FormattingEnabled = true;
            this.Branch.Location = new System.Drawing.Point(127, 123);
            this.Branch.Name = "Branch";
            this.Branch.Size = new System.Drawing.Size(100, 21);
            this.Branch.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label3.Location = new System.Drawing.Point(7, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 50);
            this.label3.TabIndex = 41;
            this.label3.Text = "Smart Cam\r\nControl Panel";
            // 
            // BalanceGrid
            // 
            this.BalanceGrid.AllowUserToAddRows = false;
            this.BalanceGrid.AllowUserToDeleteRows = false;
            this.BalanceGrid.AllowUserToOrderColumns = true;
            this.BalanceGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.BalanceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BalanceGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.BalanceGrid.Location = new System.Drawing.Point(487, 453);
            this.BalanceGrid.Name = "BalanceGrid";
            this.BalanceGrid.ReadOnly = true;
            this.BalanceGrid.Size = new System.Drawing.Size(727, 177);
            this.BalanceGrid.TabIndex = 44;
            // 
            // FromBox
            // 
            this.FromBox.AllowDrop = true;
            this.FromBox.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.FromBox.DisplayMember = "True";
            this.FromBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FromBox.FormattingEnabled = true;
            this.FromBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.FromBox.Location = new System.Drawing.Point(61, 20);
            this.FromBox.Name = "FromBox";
            this.FromBox.Size = new System.Drawing.Size(144, 21);
            this.FromBox.TabIndex = 40;
            // 
            // ToBox
            // 
            this.ToBox.AllowDrop = true;
            this.ToBox.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.ToBox.DisplayMember = "True";
            this.ToBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ToBox.FormattingEnabled = true;
            this.ToBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.ToBox.Location = new System.Drawing.Point(61, 56);
            this.ToBox.Name = "ToBox";
            this.ToBox.Size = new System.Drawing.Size(144, 21);
            this.ToBox.TabIndex = 40;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(10, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 552);
            this.panel1.TabIndex = 45;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.Transfer);
            this.panel2.Controls.Add(this.AmountBox);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.FromBox);
            this.panel2.Controls.Add(this.ToBox);
            this.panel2.Location = new System.Drawing.Point(256, 453);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(219, 177);
            this.panel2.TabIndex = 46;
            // 
            // Transfer
            // 
            this.Transfer.BackColor = System.Drawing.Color.DodgerBlue;
            this.Transfer.Location = new System.Drawing.Point(101, 138);
            this.Transfer.Name = "Transfer";
            this.Transfer.Size = new System.Drawing.Size(75, 31);
            this.Transfer.TabIndex = 50;
            this.Transfer.Text = "Transfer";
            this.Transfer.UseVisualStyleBackColor = false;
            this.Transfer.Click += new System.EventHandler(this.Transfer_Click);
            // 
            // AmountBox
            // 
            this.AmountBox.Location = new System.Drawing.Point(61, 92);
            this.AmountBox.Name = "AmountBox";
            this.AmountBox.Size = new System.Drawing.Size(144, 20);
            this.AmountBox.TabIndex = 49;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 95);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 47;
            this.label16.Text = "Amount";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 47;
            this.label10.Text = "To";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 48;
            this.label15.Text = "From";
            // 
            // StreetBox
            // 
            this.StreetBox.Location = new System.Drawing.Point(1237, 552);
            this.StreetBox.Name = "StreetBox";
            this.StreetBox.Size = new System.Drawing.Size(190, 20);
            this.StreetBox.TabIndex = 47;
            // 
            // CityBox
            // 
            this.CityBox.Location = new System.Drawing.Point(1237, 503);
            this.CityBox.Name = "CityBox";
            this.CityBox.Size = new System.Drawing.Size(190, 20);
            this.CityBox.TabIndex = 48;
            // 
            // InsertBranch
            // 
            this.InsertBranch.Location = new System.Drawing.Point(1286, 599);
            this.InsertBranch.Name = "InsertBranch";
            this.InsertBranch.Size = new System.Drawing.Size(106, 23);
            this.InsertBranch.TabIndex = 49;
            this.InsertBranch.Text = "Add new branch";
            this.InsertBranch.UseVisualStyleBackColor = true;
            this.InsertBranch.Click += new System.EventHandler(this.InsertBranch_Click);
            // 
            // Street
            // 
            this.Street.AutoSize = true;
            this.Street.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Street.Location = new System.Drawing.Point(1234, 536);
            this.Street.Name = "Street";
            this.Street.Size = new System.Drawing.Size(35, 13);
            this.Street.TabIndex = 50;
            this.Street.Text = "Street";
            // 
            // City
            // 
            this.City.AutoSize = true;
            this.City.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.City.Location = new System.Drawing.Point(1234, 487);
            this.City.Name = "City";
            this.City.Size = new System.Drawing.Size(24, 13);
            this.City.TabIndex = 51;
            this.City.Text = "City";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(1225, 453);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(218, 177);
            this.panel3.TabIndex = 52;
            // 
            // SmartCam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1455, 647);
            this.Controls.Add(this.City);
            this.Controls.Add(this.Street);
            this.Controls.Add(this.InsertBranch);
            this.Controls.Add(this.CityBox);
            this.Controls.Add(this.StreetBox);
            this.Controls.Add(this.BalanceGrid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Branch);
            this.Controls.Add(this.Sell);
            this.Controls.Add(this.HeadphoneOut);
            this.Controls.Add(this.DualCardSlot);
            this.Controls.Add(this.BuiltInFlash);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.FindBox);
            this.Controls.Add(this.ProductCode);
            this.Controls.Add(this.StillImageResolution);
            this.Controls.Add(this.VideoResolution);
            this.Controls.Add(this.Price);
            this.Controls.Add(this.SensorSize);
            this.Controls.Add(this.BodiesKits);
            this.Controls.Add(this.Model);
            this.Controls.Add(this.Brand);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Upd);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Name = "SmartCam";
            this.Text = "SCCP";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BalanceGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Upd;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Brand;
        private System.Windows.Forms.TextBox Model;
        private System.Windows.Forms.TextBox BodiesKits;
        private System.Windows.Forms.TextBox SensorSize;
        private System.Windows.Forms.TextBox Price;
        private System.Windows.Forms.TextBox VideoResolution;
        private System.Windows.Forms.TextBox StillImageResolution;
        private System.Windows.Forms.TextBox ProductCode;
        private System.Windows.Forms.TextBox FindBox;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.CheckBox BuiltInFlash;
        private System.Windows.Forms.CheckBox DualCardSlot;
        private System.Windows.Forms.CheckBox HeadphoneOut;
        private System.Windows.Forms.Button Sell;
        private System.Windows.Forms.ComboBox Branch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView BalanceGrid;
        private System.Windows.Forms.ComboBox FromBox;
        private System.Windows.Forms.ComboBox ToBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox AmountBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button Transfer;
        private System.Windows.Forms.TextBox StreetBox;
        private System.Windows.Forms.TextBox CityBox;
        private System.Windows.Forms.Button InsertBranch;
        private System.Windows.Forms.Label Street;
        private System.Windows.Forms.Label City;
        private System.Windows.Forms.Panel panel3;
    }
}

