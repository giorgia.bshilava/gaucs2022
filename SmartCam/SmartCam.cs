﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace SmartCam
{
    public partial class SmartCam : Form
    {
        SqlConnection con;
        SqlCommand cmd;


        public SmartCam()
        {
            InitializeComponent();

            con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
            cmd = new SqlCommand();

            cmd.Connection = con;
            ConCheck();


        }
        private async void ConCheck()
        {
            await Task.Run(() =>
            {
                try
                {
                    con.Open();
                    con.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());

                }
            });

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ShowStock();
            ShowBalance();
        }

        private void ShowAll_Click(object sender, EventArgs e)
        {
            ShowStock();
        }

        private void ShowSold_Click(object sender, EventArgs e)
        {
            ShowSold();

        }
        private void ShowStock()
        {
            con.Close();
            Task.Delay(10000);
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select ProductCode, City, Street, Brand, Model, [Bodies/Kits], SensorSize, Price, StillImageResolution, VideoResolution, DualCardSlot, [Built-inFlash], HeadphoneOut  from Stock join Branch on Stock.Branch = Branch.Branch_ID";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }

        private void ShowSold()
        {
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select ProductCode, City, Street, Brand, Model, [Bodies/Kits], SensorSize, Price, StillImageResolution, VideoResolution, DualCardSlot, [Built-inFlash], HeadphoneOut  from Sold join Branch on Sold.Branch = Branch.Branch_ID";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }

        private void ShowBalance()
        {
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select Branch_ID, City, Street, Amount from Balance join Branch on Balance.Branch = Branch.Branch_ID";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            BalanceGrid.DataSource = dt;
            con.Close();
        }

        private void cleardata()
        {
            ProductCode.Clear();
            Branch.ResetText();
            Brand.Clear();
            Model.Clear();
            BodiesKits.Clear();
            SensorSize.Clear();
            Price.Clear();
            StillImageResolution.Clear();
            VideoResolution.Clear();
            DualCardSlot.Checked = false;
            BuiltInFlash.Checked = false;
            HeadphoneOut.Checked = false;

        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            int row = e.RowIndex;
            ProductCode.Text = Convert.ToString(dataGridView1[0, row].Value);

            Brand.Text = Convert.ToString(dataGridView1[3, row].Value);
            Model.Text = Convert.ToString(dataGridView1[4, row].Value);
            BodiesKits.Text = Convert.ToString(dataGridView1[5, row].Value);
            SensorSize.Text = Convert.ToString(dataGridView1[6, row].Value);
            Price.Text = Convert.ToString(dataGridView1[7, row].Value);
            StillImageResolution.Text = Convert.ToString(dataGridView1[8, row].Value);
            VideoResolution.Text = Convert.ToString(dataGridView1[9, row].Value);
            DualCardSlot.Checked = Convert.ToBoolean(dataGridView1[10, row].Value);
            BuiltInFlash.Checked = Convert.ToBoolean(dataGridView1[11, row].Value);
            HeadphoneOut.Checked = Convert.ToBoolean(dataGridView1[12, row].Value);

        }




        private void Insert_Click(object sender, EventArgs e)
        {

            if (ProductCode.Text.ToString().Length == 5 && !String.IsNullOrEmpty(Branch.Text.ToString()) && !String.IsNullOrEmpty(Brand.Text.ToString()) && !String.IsNullOrEmpty(Model.Text.ToString()) && !String.IsNullOrEmpty(BodiesKits.Text.ToString()) && !String.IsNullOrEmpty(SensorSize.Text.ToString()) && !String.IsNullOrEmpty(Price.Text) && !String.IsNullOrEmpty(StillImageResolution.Text.ToString()) && !String.IsNullOrEmpty(VideoResolution.Text.ToString()))
            {
                try
                {
                    float.Parse(Price.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Enter the price correctly!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                DialogResult dialogResult = MessageBox.Show($"Would you like to add {ProductCode.Text.ToString()}?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    SqlCommand cmd = new SqlCommand("insert into Stock values(@ProductCode, @Branch, @Brand,@Model,@BodiesKits,@SensorSize,@Price,@StillImageResolution,@VideoResolution,@DualCardSlot,@BuiltInFlash,@HeadphoneOut)", con);
                    cmd.Parameters.AddWithValue("@ProductCode", ProductCode.Text.ToString());
                    cmd.Parameters.AddWithValue("@Branch", int.Parse(Branch.Text));
                    cmd.Parameters.AddWithValue("@Brand", Brand.Text.ToString());
                    cmd.Parameters.AddWithValue("@Model", Model.Text.ToString());
                    cmd.Parameters.AddWithValue("@BodiesKits", BodiesKits.Text.ToString());
                    cmd.Parameters.AddWithValue("@SensorSize", SensorSize.Text.ToString());
                    cmd.Parameters.AddWithValue("@Price", float.Parse(Price.Text));
                    cmd.Parameters.AddWithValue("@StillImageResolution", StillImageResolution.Text.ToString());
                    cmd.Parameters.AddWithValue("@VideoResolution", VideoResolution.Text.ToString());
                    cmd.Parameters.AddWithValue("@DualCardSlot", DualCardSlot.Checked);
                    cmd.Parameters.AddWithValue("@BuiltInFlash", BuiltInFlash.Checked);
                    cmd.Parameters.AddWithValue("@HeadphoneOut", HeadphoneOut.Checked);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    cleardata();
                    con.Close();
                    ShowStock();


                }
            }
            else
            {
                MessageBox.Show("Fill out the fields correctly!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void Remove_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ProductCode.Text))
            {
                DialogResult dialogResult = MessageBox.Show($"Would you like to remove {ProductCode.Text.ToString()}?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    SqlCommand cmd = new SqlCommand("delete Stock where ProductCode = @ProductCode", con);
                    cmd.Parameters.AddWithValue("@ProductCode", ProductCode.Text.ToString());
                    con.Open();
                    if (cmd.ExecuteNonQuery() == 0)
                    {
                        MessageBox.Show("There are no such products", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    dataGridView1.DataSource = cmd;
                    cleardata();
                    con.Close();
                    ShowStock();
                }
            }

        }



        private void Upd_Click(object sender, EventArgs e)
        {

            if (ProductCode.Text.ToString().Length == 5 && !String.IsNullOrEmpty(Branch.Text.ToString()) && !String.IsNullOrEmpty(Brand.Text.ToString()) && !String.IsNullOrEmpty(Model.Text.ToString()) && !String.IsNullOrEmpty(BodiesKits.Text.ToString()) && !String.IsNullOrEmpty(SensorSize.Text.ToString()) && !String.IsNullOrEmpty(Price.Text) && !String.IsNullOrEmpty(StillImageResolution.Text.ToString()) && !String.IsNullOrEmpty(VideoResolution.Text.ToString()))
            {
                try
                {
                    float.Parse(Price.Text);

                }
                catch (Exception)
                {
                    MessageBox.Show("Enter the price correctly!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                DialogResult dialogResult = MessageBox.Show($"Would you like to update {ProductCode.Text.ToString()}?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    con.Open();


                    SqlCommand cmd = new SqlCommand($"update Stock set Branch = @Branch, Brand = @Brand, Model = @Model, [Bodies/Kits] = @BodiesKits, SensorSize =  @SensorSize, Price = @Price, StillImageResolution = @StillImageResolution, VideoResolution = @VideoResolution, DualCardSlot = @DualCardSlot, [Built-inFlash] = @BuiltInFlash, HeadphoneOut = @HeadphoneOut  where ProductCode = @ProductCode", con);


                    cmd.Parameters.AddWithValue("@ProductCode", ProductCode.Text.ToString());
                    cmd.Parameters.AddWithValue("@Branch", int.Parse(Branch.Text));
                    cmd.Parameters.AddWithValue("@Brand", Brand.Text.ToString());
                    cmd.Parameters.AddWithValue("@Model", Model.Text.ToString());
                    cmd.Parameters.AddWithValue("@BodiesKits", BodiesKits.Text.ToString());
                    cmd.Parameters.AddWithValue("@SensorSize", SensorSize.Text.ToString());
                    cmd.Parameters.AddWithValue("@Price", float.Parse(Price.Text));
                    cmd.Parameters.AddWithValue("@StillImageResolution", StillImageResolution.Text.ToString());
                    cmd.Parameters.AddWithValue("@VideoResolution", VideoResolution.Text.ToString());
                    cmd.Parameters.AddWithValue("@DualCardSlot", DualCardSlot.Checked);
                    cmd.Parameters.AddWithValue("@BuiltInFlash", BuiltInFlash.Checked);
                    cmd.Parameters.AddWithValue("@HeadphoneOut", HeadphoneOut.Checked);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    ShowStock();
                    cleardata();
                }
            }
            else
            {
                MessageBox.Show("Fill out the fields correctly!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }






        private void Find_Click(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"select ProductCode, City, Street, Brand, Model, [Bodies/Kits], SensorSize, Price, StillImageResolution, VideoResolution, DualCardSlot, [Built-inFlash], HeadphoneOut  from Stock join Branch on Stock.Branch = Branch.Branch_ID where ProductCode LIKE  '{FindBox.Text}%'";
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            ProductCode.Text = dt.ToString();
            Model.Text = dt.ToString();
            Price.Text = dt.ToString();
            dataGridView1.DataSource = dt;
            con.Close();
        }



        private void Sell_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ProductCode.Text))
            {
                DialogResult dialogResult = MessageBox.Show($"Would you like to sell {ProductCode.Text.ToString()}?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {

                    SqlCommand cmd = new SqlCommand("Sell", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@ProductCode", ProductCode.Text);


                    con.Open();
                    if (cmd.ExecuteNonQuery() == 0)
                    {
                        MessageBox.Show("There are no such products", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    cleardata();
                    con.Close();
                    ShowStock();
                }
            }
        }


        SqlTransaction tran;

        private void Transfer_Click(object sender, EventArgs e)
        {
            try
            {

                con.Open();
                tran = con.BeginTransaction();
                SqlCommand cmd = new SqlCommand("update Balance set Amount = Amount -  @Amount where Branch = @Branch", con, tran);
                cmd.Parameters.AddWithValue("@Amount", AmountBox.Text);
                cmd.Parameters.AddWithValue("@Branch", FromBox.Text);
                cmd.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("update Balance set Amount = Amount +  @Amount where Branch = @Branch", con, tran);
                cmd2.Parameters.AddWithValue("@Amount", AmountBox.Text);
                cmd2.Parameters.AddWithValue("@Branch", ToBox.Text);
                cmd2.ExecuteNonQuery();

                tran.Commit();
                con.Close();
                ShowBalance();
            }


            catch (Exception)
            {

                MessageBox.Show("Error", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void InsertBranch_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable("Branch");
            DataRow dr = dt.NewRow();
            SqlDataAdapter da = new SqlDataAdapter("select * from Branch", con);
            da.Fill(dt);
      
            dr[1] = CityBox.Text;
            dr[2] = StreetBox.Text;

            dt.Rows.Add(dr);
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            SqlCommandBuilder builder = new SqlCommandBuilder(da);

            try
            {
                builder.GetUpdateCommand();
                da.Update(ds, "Branch");
                ds.Tables.Clear();
                MessageBox.Show($"New Branch {CityBox.Text} {StreetBox.Text} added!", "Sucsess!");
                CityBox.Clear();
                StreetBox.Clear();


            }
            catch (Exception)
            {
                MessageBox.Show("Error!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 
            }
            con.Close();


        }

       
    }
}
