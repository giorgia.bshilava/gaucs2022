﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Data.Entity.Migrations;

namespace Quiz2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Model modelContext = new Model();

        void GetCustomers()
        {
            var customers = modelContext.Customers.ToList();
            CustomerDataGridView.DataSource = customers;
            CustomerDataGridView.BringToFront();
            CustomerPanel.BringToFront();

        }
        void GetOrders()
        {
            var orders = modelContext.Orders.Join(modelContext.Customers, order => order.CustomerId,
        customer => customer.Id, (order, customer) => new
        {

             Id = order.Id,
             OrderDate = order.OrderDate,
             OrderNumber = order.OrderNumber,
             FirstName = customer.FirstName,
             LastName = customer.LastName, 
             TotalAmount= order.TotalAmount,




        });
            OrderDataGridView.DataSource = orders.ToList();
            OrderDataGridView.BringToFront();
            OrderPanel.BringToFront();

        }
        void GetOrderItems()
        {
            var orderItems = modelContext.OrderItems
                .Join(modelContext.Products, orderItem => orderItem.ProductId,
            product => product.Id, (orderItem, product) => new
            {
                Id = orderItem.Id,
                ProductName = product.ProductName,

                UnitPrice = orderItem.UnitPrice,
                Package = product.Package,
                Quantity = orderItem.Quantity,
            });
            orderItems.Join(modelContext.Orders, orderItem => orderItem.Id,
        order => order.Id, (orderItem, order) => new
        {

            OrderNumber = order.OrderNumber,



        });
            OrderItemDataGridView.DataSource = orderItems.ToList();
            OrderItemDataGridView.BringToFront();
            OrderItemPanel.BringToFront();

        }
        void GetProducts()
        {

            var products = modelContext.Products.Join(modelContext.Suppliers, product => product.SupplierId,
            supplier => supplier.Id, (product, supplier) => new
            {
                Id = product.Id,
                ProductName = product.ProductName,
                Supplier = supplier.CompanyName,
                UnitPrice = product.UnitPrice,
                Package = product.Package,
                IsDiscontinued = product.IsDiscontinued,


            });
            ProductDataGridView.DataSource = products.ToList();
            ProductDataGridView.BringToFront();
            ProductPanel.BringToFront();

        }
        void GetSuppliers()
        {
            var suppliers = modelContext.Suppliers.ToList();
            SupplierDataGridView.DataSource = suppliers;
            SupplierDataGridView.BringToFront();
            SupplierPanel.BringToFront();

        }



        private void SelectTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectTable.Text.ToString() == "Customer")
            {
                GetCustomers();
            }
            if (SelectTable.Text.ToString() == "Order")
            {
                GetOrders();
            }
            if (SelectTable.Text.ToString() == "OrderItem")
            {
                GetOrderItems();
            }
            if (SelectTable.Text.ToString() == "Product")
            {
                GetProducts();
            }
            if (SelectTable.Text.ToString() == "Supplier")
            {
                GetSuppliers();
            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SelectTable.Text = "Customer";
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (SelectTable.Text.ToString() == "Supplier")
            {
                Supplier supplier = new Supplier()
                {
                    CompanyName = CompanyName.Text.ToString(),
                    ContactName = ContactName.Text.ToString(),
                    ContactTitle = ContactTitle.Text.ToString(),
                    City = SupplierCity.Text.ToString(),
                    Country = SupplierCountry.Text.ToString(),
                    Phone = SupplierPhone.Text.ToString(),
                    Fax = SupplierFax.Text.ToString(),
                };

                modelContext.Suppliers.Add(supplier);
                modelContext.SaveChanges();
                GetSuppliers();
            }
            if (SelectTable.Text.ToString() == "Product")
            {
                Product product = new Product()
                {
                    ProductName = ProductName.Text.ToString(),
                    SupplierId = int.Parse(SupplierIdP.Text),
                    UnitPrice = decimal.Parse(UnitPriceP.Text),
                    Package = Package.Text.ToString(),
                    IsDiscontinued = bool.Parse(IsDiscontinued.Text),

                };

                modelContext.Products.Add(product);
                modelContext.SaveChanges();
                GetProducts();
            }
            if (SelectTable.Text.ToString() == "OrderItem")
            {
                OrderItem orderitem = new OrderItem()
                {
                    OrderId = int.Parse(OrderIdO.Text),
                    ProductId = int.Parse(ProductIdO.Text),
                    UnitPrice = decimal.Parse(UnitPriceO.Text),
                    Quantity = int.Parse(Quantity.Text),


                };

                modelContext.OrderItems.Add(orderitem);
                modelContext.SaveChanges();
                GetOrderItems();
            }
            if (SelectTable.Text.ToString() == "Order")
            {
                Order order = new Order()
                {
                    OrderDate = DateTime.Parse(OrderDate.Text),
                    OrderNumber = OrderNumber.Text.ToString(),
                    CustomerId = int.Parse(CustomerIdO.Text),
                    TotalAmount = decimal.Parse(TotalAmount.Text),



                };

                modelContext.Orders.Add(order);
                modelContext.SaveChanges();
                GetOrders();
            }
            if (SelectTable.Text.ToString() == "Customer")
            {
                Customer customer = new Customer()
                {
                    FirstName = FirstName.Text.ToString(),
                    LastName = LastName.Text.ToString(),
                    City = CityC.Text.ToString(),
                    Country = CountryC.Text.ToString(),
                    Phone = PhoneC.Text.ToString(),



                };

                modelContext.Customers.Add(customer);
                modelContext.SaveChanges();
                GetCustomers();
            }

        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (SelectTable.Text.ToString() == "Supplier")
            {
                int ID = int.Parse(SupplierId.Text);
                Supplier supplier = modelContext.Suppliers.FirstOrDefault(x => x.Id == ID);
                modelContext.Suppliers.Remove(supplier);
                modelContext.SaveChanges();
                GetSuppliers();
            }
            if (SelectTable.Text.ToString() == "Product")
            {
                int ID = int.Parse(ProductId.Text);
                Product product = modelContext.Products.FirstOrDefault(x => x.Id == ID);
                modelContext.Products.Remove(product);
                modelContext.SaveChanges();
                GetProducts();
            }
            if (SelectTable.Text.ToString() == "OrderItem")
            {
                int ID = int.Parse(OrderItemId.Text);
                OrderItem orderItem = modelContext.OrderItems.FirstOrDefault(x => x.Id == ID);
                modelContext.OrderItems.Remove(orderItem);
                modelContext.SaveChanges();
                GetOrderItems();
            }
            if (SelectTable.Text.ToString() == "Order")
            {
                int ID = int.Parse(OrderId.Text);
                Order order = modelContext.Orders.FirstOrDefault(x => x.Id == ID);
                modelContext.Orders.Remove(order);
                modelContext.SaveChanges();
                GetOrders();
            }
            if (SelectTable.Text.ToString() == "Customer")
            {
                int ID = int.Parse(CustomerId.Text);
                Customer customer = modelContext.Customers.FirstOrDefault(x => x.Id == ID);
                modelContext.Customers.Remove(customer);
                modelContext.SaveChanges();
                GetCustomers();
            }


        }

        private void Edit_Click(object sender, EventArgs e)
        {
            if (SelectTable.Text.ToString() == "Supplier")
            {
                Supplier supplier = new Supplier()
                {
                    Id = int.Parse(SupplierId.Text),
                    CompanyName = CompanyName.Text.ToString(),
                    ContactName = ContactName.Text.ToString(),
                    ContactTitle = ContactTitle.Text.ToString(),
                    City = SupplierCity.Text.ToString(),
                    Country = SupplierCountry.Text.ToString(),
                    Phone = SupplierPhone.Text.ToString(),
                    Fax = SupplierFax.Text.ToString(),
                };

                modelContext.Suppliers.AddOrUpdate(supplier);
                modelContext.SaveChanges();
                GetSuppliers();
            }
            if (SelectTable.Text.ToString() == "Product")
            {
                Product product = new Product()
                {
                    Id = int.Parse(ProductId.Text),
                    ProductName = ProductName.Text.ToString(),
                    SupplierId = int.Parse(SupplierIdP.Text),
                    UnitPrice = decimal.Parse(UnitPriceP.Text),
                    Package = Package.Text.ToString(),
                    IsDiscontinued = bool.Parse(IsDiscontinued.Text),

                };

                modelContext.Products.AddOrUpdate(product);
                modelContext.SaveChanges();
                GetProducts();
            }
            if (SelectTable.Text.ToString() == "OrderItem")
            {
                OrderItem orderitem = new OrderItem()
                {
                    Id = int.Parse(OrderItemId.Text),
                    OrderId = int.Parse(OrderItemId.Text),
                    ProductId = int.Parse(ProductIdO.Text),
                    UnitPrice = decimal.Parse(UnitPriceO.Text),
                    Quantity = int.Parse(Quantity.Text),


                };

                modelContext.OrderItems.AddOrUpdate(orderitem);
                modelContext.SaveChanges();
                GetOrderItems();
            }
            if (SelectTable.Text.ToString() == "Order")
            {
                Order order = new Order()
                {
                    Id = int.Parse(OrderId.Text),
                    OrderDate = DateTime.Parse(OrderDate.Text),
                    OrderNumber = OrderNumber.Text.ToString(),
                    CustomerId = int.Parse(CustomerIdO.Text),
                    TotalAmount = decimal.Parse(TotalAmount.Text),



                };

                modelContext.Orders.AddOrUpdate(order);
                modelContext.SaveChanges();
                GetOrders();
            }
            if (SelectTable.Text.ToString() == "Customer")
            {
                Customer customer = new Customer()
                {
                    Id = int.Parse(CustomerId.Text),
                    FirstName = FirstName.Text.ToString(),
                    LastName = LastName.Text.ToString(),
                    City = CityC.Text.ToString(),
                    Country = CountryC.Text.ToString(),
                    Phone = PhoneC.Text.ToString(),



                };

                modelContext.Customers.AddOrUpdate(customer);
                modelContext.SaveChanges();
                GetCustomers();
            }
        }


    }
}
