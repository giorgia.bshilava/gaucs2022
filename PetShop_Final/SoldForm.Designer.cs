﻿namespace PetShop_Final
{
    partial class SoldForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StockGridView = new System.Windows.Forms.DataGridView();
            this.IdBox = new System.Windows.Forms.TextBox();
            this.ProductNameBox = new System.Windows.Forms.TextBox();
            this.AmountBox = new System.Windows.Forms.TextBox();
            this.SupplierBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PriceBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Delete = new System.Windows.Forms.Button();
            this.Sell = new System.Windows.Forms.Button();
            this.Edit = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SelectBox = new System.Windows.Forms.ComboBox();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.Find = new System.Windows.Forms.Button();
            this.ShowReport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.StockGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // StockGridView
            // 
            this.StockGridView.BackgroundColor = System.Drawing.Color.BlanchedAlmond;
            this.StockGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.StockGridView.EnableHeadersVisualStyles = false;
            this.StockGridView.Location = new System.Drawing.Point(207, 39);
            this.StockGridView.Name = "StockGridView";
            this.StockGridView.RowHeadersWidth = 50;
            this.StockGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.StockGridView.RowTemplate.Height = 35;
            this.StockGridView.RowTemplate.ReadOnly = true;
            this.StockGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.StockGridView.Size = new System.Drawing.Size(1033, 595);
            this.StockGridView.TabIndex = 0;
            this.StockGridView.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.StockGridView_DoubleClick);
            // 
            // IdBox
            // 
            this.IdBox.Location = new System.Drawing.Point(104, 12);
            this.IdBox.Name = "IdBox";
            this.IdBox.Size = new System.Drawing.Size(97, 20);
            this.IdBox.TabIndex = 1;
            // 
            // ProductNameBox
            // 
            this.ProductNameBox.Location = new System.Drawing.Point(104, 38);
            this.ProductNameBox.Name = "ProductNameBox";
            this.ProductNameBox.Size = new System.Drawing.Size(97, 20);
            this.ProductNameBox.TabIndex = 2;
            // 
            // AmountBox
            // 
            this.AmountBox.Location = new System.Drawing.Point(104, 64);
            this.AmountBox.Name = "AmountBox";
            this.AmountBox.Size = new System.Drawing.Size(97, 20);
            this.AmountBox.TabIndex = 3;
            // 
            // SupplierBox
            // 
            this.SupplierBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SupplierBox.FormattingEnabled = true;
            this.SupplierBox.Location = new System.Drawing.Point(104, 116);
            this.SupplierBox.Name = "SupplierBox";
            this.SupplierBox.Size = new System.Drawing.Size(97, 21);
            this.SupplierBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Product Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Price";
            // 
            // PriceBox
            // 
            this.PriceBox.Location = new System.Drawing.Point(104, 90);
            this.PriceBox.Name = "PriceBox";
            this.PriceBox.Size = new System.Drawing.Size(97, 20);
            this.PriceBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Supplier";
            // 
            // Delete
            // 
            this.Delete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.Delete.Location = new System.Drawing.Point(12, 215);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(83, 30);
            this.Delete.TabIndex = 9;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = false;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Sell
            // 
            this.Sell.BackColor = System.Drawing.Color.PaleGreen;
            this.Sell.Location = new System.Drawing.Point(106, 215);
            this.Sell.Name = "Sell";
            this.Sell.Size = new System.Drawing.Size(97, 30);
            this.Sell.TabIndex = 10;
            this.Sell.Text = "Return to stock";
            this.Sell.UseVisualStyleBackColor = false;
            this.Sell.Click += new System.EventHandler(this.Sell_Click);
            // 
            // Edit
            // 
            this.Edit.BackColor = System.Drawing.Color.Linen;
            this.Edit.Location = new System.Drawing.Point(104, 179);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(97, 30);
            this.Edit.TabIndex = 11;
            this.Edit.Text = "Edit";
            this.Edit.UseVisualStyleBackColor = false;
            this.Edit.Click += new System.EventHandler(this.Edit_Click);
            // 
            // Add
            // 
            this.Add.BackColor = System.Drawing.Color.Bisque;
            this.Add.Location = new System.Drawing.Point(12, 179);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(83, 30);
            this.Add.TabIndex = 12;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = false;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift SemiLight SemiConde", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 491);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(193, 28);
            this.label6.TabIndex = 13;
            this.label6.Text = "Select table to view:";
            // 
            // SelectBox
            // 
            this.SelectBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectBox.FormattingEnabled = true;
            this.SelectBox.Items.AddRange(new object[] {
            "Stock",
            "Sold",
            "Users"});
            this.SelectBox.Location = new System.Drawing.Point(12, 532);
            this.SelectBox.Name = "SelectBox";
            this.SelectBox.Size = new System.Drawing.Size(188, 21);
            this.SelectBox.TabIndex = 14;
            this.SelectBox.SelectedIndexChanged += new System.EventHandler(this.SelectBox_SelectedIndexChanged);
            // 
            // SearchBox
            // 
            this.SearchBox.Location = new System.Drawing.Point(207, 12);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(929, 20);
            this.SearchBox.TabIndex = 15;
            // 
            // Find
            // 
            this.Find.BackColor = System.Drawing.Color.SkyBlue;
            this.Find.Location = new System.Drawing.Point(1142, 6);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(98, 30);
            this.Find.TabIndex = 16;
            this.Find.Text = "Find";
            this.Find.UseVisualStyleBackColor = false;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // ShowReport
            // 
            this.ShowReport.Location = new System.Drawing.Point(12, 251);
            this.ShowReport.Name = "ShowReport";
            this.ShowReport.Size = new System.Drawing.Size(188, 30);
            this.ShowReport.TabIndex = 17;
            this.ShowReport.Text = "Show Report";
            this.ShowReport.UseVisualStyleBackColor = true;
            this.ShowReport.Click += new System.EventHandler(this.ShowReport_Click);
            // 
            // SoldForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightYellow;
            this.ClientSize = new System.Drawing.Size(1252, 646);
            this.Controls.Add(this.ShowReport);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.SelectBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.Edit);
            this.Controls.Add(this.Sell);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SupplierBox);
            this.Controls.Add(this.PriceBox);
            this.Controls.Add(this.AmountBox);
            this.Controls.Add(this.ProductNameBox);
            this.Controls.Add(this.IdBox);
            this.Controls.Add(this.StockGridView);
            this.Name = "SoldForm";
            this.Text = "Sold";
            ((System.ComponentModel.ISupportInitialize)(this.StockGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView StockGridView;
        private System.Windows.Forms.TextBox IdBox;
        private System.Windows.Forms.TextBox ProductNameBox;
        private System.Windows.Forms.TextBox AmountBox;
        private System.Windows.Forms.ComboBox SupplierBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox PriceBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Sell;
        private System.Windows.Forms.Button Edit;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox SelectBox;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.Button ShowReport;
    }
}