﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Linq
{
    public partial class Form1 : Form
    {
        SqlConnection con;
        SqlCommand cmd;

        private linqEntities db = new linqEntities();
        private object crud;

        
        public Form1()
        {
            InitializeComponent();

            
            cmd = new SqlCommand();

            cmd.Connection = con;
           
        }

        private void ShowProducts_Click(object sender, EventArgs e)
        {
            GetProducts();
        }
        public void GetProducts()
        {
            List<SelectWithJoin> productsList = db.SelectWithJoins.ToList();
            Grid1.DataSource = productsList;
        }

        private void ShowProductTypes_Click(object sender, EventArgs e)
        {
            GetProductTypes();
        }
        public void GetProductTypes()
        {
            List<ProductType> ProductTypesList = db.ProductTypes.ToList();
            Grid1.DataSource = ProductTypesList;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {

             
            Product product = new Product()
            {
                Product_Name = ProductNameBox.Text.ToString(),
                Manufacturer = ManufacturerBox.Text.ToString(),
                Price = int.Parse(PriceBox.Text),
                Product_Type_ID = int.Parse(ProductTypeBox.Text),

            };

            AddProduct(product);
            GetProducts();


        }
        public void AddProduct(Product Product)
        {
            db.Products.Add(Product);
            db.SaveChanges();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            Product product = new Product()
            {
                Product_ID = int.Parse(Product_IDBox.Text),
                Product_Name = ProductNameBox.Text.ToString(),
                Manufacturer = ManufacturerBox.Text.ToString(),
                Price = int.Parse(PriceBox.Text),
                Product_Type_ID = int.Parse(ProductTypeBox.Text),

            };

            UpdateProduct(product);
            GetProducts();
        }
        public void UpdateProduct(Product Product)
        {
            db.Products.AddOrUpdate(Product);
            db.SaveChanges();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            int ID = int.Parse(Product_IDBox.Text);
            DeleteProduct(ID);
            GetProducts();
        }
        public void DeleteProduct(int ID)
        {
            Product product = db.Products.FirstOrDefault(x => x.Product_ID == ID);
            db.Products.Remove(product);

            db.SaveChanges();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetProducts();
        }
    }

     
}
