﻿namespace Quiz2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectTable = new System.Windows.Forms.ComboBox();
            this.CustomerPanel = new System.Windows.Forms.Panel();
            this.LastName = new System.Windows.Forms.TextBox();
            this.FirstName = new System.Windows.Forms.TextBox();
            this.CustomerId = new System.Windows.Forms.TextBox();
            this.CityC = new System.Windows.Forms.TextBox();
            this.PhoneC = new System.Windows.Forms.TextBox();
            this.CountryC = new System.Windows.Forms.TextBox();
            this.Customer = new System.Windows.Forms.Label();
            this.OrderPanel = new System.Windows.Forms.Panel();
            this.OrderDate = new System.Windows.Forms.DateTimePicker();
            this.OrderNumber = new System.Windows.Forms.TextBox();
            this.CustomerIdO = new System.Windows.Forms.TextBox();
            this.OrderId = new System.Windows.Forms.TextBox();
            this.TotalAmount = new System.Windows.Forms.TextBox();
            this.OrderItemPanel = new System.Windows.Forms.Panel();
            this.UnitPriceO = new System.Windows.Forms.TextBox();
            this.ProductIdO = new System.Windows.Forms.TextBox();
            this.OrderIdO = new System.Windows.Forms.TextBox();
            this.OrderItemId = new System.Windows.Forms.TextBox();
            this.Quantity = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SupplierPanel = new System.Windows.Forms.Panel();
            this.SupplierFax = new System.Windows.Forms.TextBox();
            this.SupplierPhone = new System.Windows.Forms.TextBox();
            this.SupplierCountry = new System.Windows.Forms.TextBox();
            this.SupplierCity = new System.Windows.Forms.TextBox();
            this.ContactTitle = new System.Windows.Forms.TextBox();
            this.ContactName = new System.Windows.Forms.TextBox();
            this.CompanyName = new System.Windows.Forms.TextBox();
            this.SupplierId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ProductPanel = new System.Windows.Forms.Panel();
            this.IsDiscontinued = new System.Windows.Forms.ComboBox();
            this.UnitPriceP = new System.Windows.Forms.TextBox();
            this.Package = new System.Windows.Forms.TextBox();
            this.SupplierIdP = new System.Windows.Forms.TextBox();
            this.ProductName = new System.Windows.Forms.TextBox();
            this.ProductId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Add = new System.Windows.Forms.Button();
            this.Edit = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.CustomerDataGridView = new System.Windows.Forms.DataGridView();
            this.OrderDataGridView = new System.Windows.Forms.DataGridView();
            this.OrderItemDataGridView = new System.Windows.Forms.DataGridView();
            this.ProductDataGridView = new System.Windows.Forms.DataGridView();
            this.SupplierDataGridView = new System.Windows.Forms.DataGridView();
            this.CustomerPanel.SuspendLayout();
            this.OrderPanel.SuspendLayout();
            this.OrderItemPanel.SuspendLayout();
            this.SupplierPanel.SuspendLayout();
            this.ProductPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderItemDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // SelectTable
            // 
            this.SelectTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectTable.FormattingEnabled = true;
            this.SelectTable.Items.AddRange(new object[] {
            "Customer",
            "Order",
            "OrderItem",
            "Product",
            "Supplier"});
            this.SelectTable.Location = new System.Drawing.Point(12, 12);
            this.SelectTable.Name = "SelectTable";
            this.SelectTable.Size = new System.Drawing.Size(121, 21);
            this.SelectTable.TabIndex = 0;
            this.SelectTable.SelectedIndexChanged += new System.EventHandler(this.SelectTable_SelectedIndexChanged);
            // 
            // CustomerPanel
            // 
            this.CustomerPanel.BackColor = System.Drawing.SystemColors.Control;
            this.CustomerPanel.Controls.Add(this.LastName);
            this.CustomerPanel.Controls.Add(this.FirstName);
            this.CustomerPanel.Controls.Add(this.CustomerId);
            this.CustomerPanel.Controls.Add(this.CityC);
            this.CustomerPanel.Controls.Add(this.PhoneC);
            this.CustomerPanel.Controls.Add(this.CountryC);
            this.CustomerPanel.Controls.Add(this.Customer);
            this.CustomerPanel.Location = new System.Drawing.Point(12, 39);
            this.CustomerPanel.Name = "CustomerPanel";
            this.CustomerPanel.Size = new System.Drawing.Size(121, 341);
            this.CustomerPanel.TabIndex = 2;
            // 
            // LastName
            // 
            this.LastName.Location = new System.Drawing.Point(1, 87);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(121, 20);
            this.LastName.TabIndex = 9;
            this.LastName.Text = "Last Name";
            // 
            // FirstName
            // 
            this.FirstName.Location = new System.Drawing.Point(0, 61);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(121, 20);
            this.FirstName.TabIndex = 8;
            this.FirstName.Text = "First Name";
            // 
            // CustomerId
            // 
            this.CustomerId.Location = new System.Drawing.Point(0, 35);
            this.CustomerId.Name = "CustomerId";
            this.CustomerId.Size = new System.Drawing.Size(121, 20);
            this.CustomerId.TabIndex = 7;
            this.CustomerId.Text = "Id";
            // 
            // CityC
            // 
            this.CityC.Location = new System.Drawing.Point(0, 113);
            this.CityC.Name = "CityC";
            this.CityC.Size = new System.Drawing.Size(121, 20);
            this.CityC.TabIndex = 6;
            this.CityC.Text = "City";
            // 
            // PhoneC
            // 
            this.PhoneC.Location = new System.Drawing.Point(0, 165);
            this.PhoneC.Name = "PhoneC";
            this.PhoneC.Size = new System.Drawing.Size(121, 20);
            this.PhoneC.TabIndex = 5;
            this.PhoneC.Text = "Phone";
            // 
            // CountryC
            // 
            this.CountryC.Location = new System.Drawing.Point(0, 139);
            this.CountryC.Name = "CountryC";
            this.CountryC.Size = new System.Drawing.Size(121, 20);
            this.CountryC.TabIndex = 5;
            this.CountryC.Text = "Country";
            // 
            // Customer
            // 
            this.Customer.AutoSize = true;
            this.Customer.Location = new System.Drawing.Point(-2, 9);
            this.Customer.Name = "Customer";
            this.Customer.Size = new System.Drawing.Size(51, 13);
            this.Customer.TabIndex = 3;
            this.Customer.Text = "Customer";
            // 
            // OrderPanel
            // 
            this.OrderPanel.Controls.Add(this.OrderDate);
            this.OrderPanel.Controls.Add(this.OrderNumber);
            this.OrderPanel.Controls.Add(this.CustomerIdO);
            this.OrderPanel.Controls.Add(this.OrderId);
            this.OrderPanel.Controls.Add(this.TotalAmount);
            this.OrderPanel.Controls.Add(this.label1);
            this.OrderPanel.Location = new System.Drawing.Point(12, 39);
            this.OrderPanel.Name = "OrderPanel";
            this.OrderPanel.Size = new System.Drawing.Size(121, 341);
            this.OrderPanel.TabIndex = 4;
            // 
            // OrderDate
            // 
            this.OrderDate.Location = new System.Drawing.Point(0, 63);
            this.OrderDate.Name = "OrderDate";
            this.OrderDate.Size = new System.Drawing.Size(122, 20);
            this.OrderDate.TabIndex = 11;
            // 
            // OrderNumber
            // 
            this.OrderNumber.Location = new System.Drawing.Point(0, 87);
            this.OrderNumber.Name = "OrderNumber";
            this.OrderNumber.Size = new System.Drawing.Size(122, 20);
            this.OrderNumber.TabIndex = 10;
            this.OrderNumber.Text = "OrderNumber";
            // 
            // CustomerIdO
            // 
            this.CustomerIdO.Location = new System.Drawing.Point(0, 113);
            this.CustomerIdO.Name = "CustomerIdO";
            this.CustomerIdO.Size = new System.Drawing.Size(122, 20);
            this.CustomerIdO.TabIndex = 9;
            this.CustomerIdO.Text = "Customer Id";
            // 
            // OrderId
            // 
            this.OrderId.Location = new System.Drawing.Point(0, 37);
            this.OrderId.Name = "OrderId";
            this.OrderId.Size = new System.Drawing.Size(122, 20);
            this.OrderId.TabIndex = 7;
            this.OrderId.Text = "Id";
            // 
            // TotalAmount
            // 
            this.TotalAmount.Location = new System.Drawing.Point(0, 139);
            this.TotalAmount.Name = "TotalAmount";
            this.TotalAmount.Size = new System.Drawing.Size(122, 20);
            this.TotalAmount.TabIndex = 6;
            this.TotalAmount.Text = "Total Amount";
            // 
            // OrderItemPanel
            // 
            this.OrderItemPanel.Controls.Add(this.UnitPriceO);
            this.OrderItemPanel.Controls.Add(this.ProductIdO);
            this.OrderItemPanel.Controls.Add(this.OrderIdO);
            this.OrderItemPanel.Controls.Add(this.OrderItemId);
            this.OrderItemPanel.Controls.Add(this.Quantity);
            this.OrderItemPanel.Controls.Add(this.label2);
            this.OrderItemPanel.Location = new System.Drawing.Point(12, 39);
            this.OrderItemPanel.Name = "OrderItemPanel";
            this.OrderItemPanel.Size = new System.Drawing.Size(121, 341);
            this.OrderItemPanel.TabIndex = 5;
            // 
            // UnitPriceO
            // 
            this.UnitPriceO.Location = new System.Drawing.Point(0, 113);
            this.UnitPriceO.Name = "UnitPriceO";
            this.UnitPriceO.Size = new System.Drawing.Size(121, 20);
            this.UnitPriceO.TabIndex = 8;
            this.UnitPriceO.Text = "Unit Price";
            // 
            // ProductIdO
            // 
            this.ProductIdO.Location = new System.Drawing.Point(0, 87);
            this.ProductIdO.Name = "ProductIdO";
            this.ProductIdO.Size = new System.Drawing.Size(121, 20);
            this.ProductIdO.TabIndex = 7;
            this.ProductIdO.Text = "Product Id";
            // 
            // OrderIdO
            // 
            this.OrderIdO.Location = new System.Drawing.Point(0, 61);
            this.OrderIdO.Name = "OrderIdO";
            this.OrderIdO.Size = new System.Drawing.Size(121, 20);
            this.OrderIdO.TabIndex = 6;
            this.OrderIdO.Text = "Order Id";
            // 
            // OrderItemId
            // 
            this.OrderItemId.Location = new System.Drawing.Point(0, 35);
            this.OrderItemId.Name = "OrderItemId";
            this.OrderItemId.Size = new System.Drawing.Size(121, 20);
            this.OrderItemId.TabIndex = 5;
            this.OrderItemId.Text = "Id";
            // 
            // Quantity
            // 
            this.Quantity.Location = new System.Drawing.Point(0, 139);
            this.Quantity.Name = "Quantity";
            this.Quantity.Size = new System.Drawing.Size(121, 20);
            this.Quantity.TabIndex = 4;
            this.Quantity.Text = "Quantity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-3, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "OrderItem";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Order";
            // 
            // SupplierPanel
            // 
            this.SupplierPanel.Controls.Add(this.SupplierFax);
            this.SupplierPanel.Controls.Add(this.SupplierPhone);
            this.SupplierPanel.Controls.Add(this.SupplierCountry);
            this.SupplierPanel.Controls.Add(this.SupplierCity);
            this.SupplierPanel.Controls.Add(this.ContactTitle);
            this.SupplierPanel.Controls.Add(this.ContactName);
            this.SupplierPanel.Controls.Add(this.CompanyName);
            this.SupplierPanel.Controls.Add(this.SupplierId);
            this.SupplierPanel.Controls.Add(this.label4);
            this.SupplierPanel.Location = new System.Drawing.Point(12, 39);
            this.SupplierPanel.Name = "SupplierPanel";
            this.SupplierPanel.Size = new System.Drawing.Size(121, 341);
            this.SupplierPanel.TabIndex = 7;
            // 
            // SupplierFax
            // 
            this.SupplierFax.Location = new System.Drawing.Point(0, 217);
            this.SupplierFax.Name = "SupplierFax";
            this.SupplierFax.Size = new System.Drawing.Size(121, 20);
            this.SupplierFax.TabIndex = 4;
            this.SupplierFax.Text = "Fax";
            // 
            // SupplierPhone
            // 
            this.SupplierPhone.Location = new System.Drawing.Point(0, 191);
            this.SupplierPhone.Name = "SupplierPhone";
            this.SupplierPhone.Size = new System.Drawing.Size(121, 20);
            this.SupplierPhone.TabIndex = 4;
            this.SupplierPhone.Text = "Phone";
            // 
            // SupplierCountry
            // 
            this.SupplierCountry.Location = new System.Drawing.Point(0, 165);
            this.SupplierCountry.Name = "SupplierCountry";
            this.SupplierCountry.Size = new System.Drawing.Size(121, 20);
            this.SupplierCountry.TabIndex = 4;
            this.SupplierCountry.Text = "Country";
            // 
            // SupplierCity
            // 
            this.SupplierCity.Location = new System.Drawing.Point(0, 139);
            this.SupplierCity.Name = "SupplierCity";
            this.SupplierCity.Size = new System.Drawing.Size(121, 20);
            this.SupplierCity.TabIndex = 4;
            this.SupplierCity.Text = "City";
            // 
            // ContactTitle
            // 
            this.ContactTitle.Location = new System.Drawing.Point(0, 113);
            this.ContactTitle.Name = "ContactTitle";
            this.ContactTitle.Size = new System.Drawing.Size(121, 20);
            this.ContactTitle.TabIndex = 4;
            this.ContactTitle.Text = "Contact Title";
            // 
            // ContactName
            // 
            this.ContactName.Location = new System.Drawing.Point(0, 87);
            this.ContactName.Name = "ContactName";
            this.ContactName.Size = new System.Drawing.Size(121, 20);
            this.ContactName.TabIndex = 4;
            this.ContactName.Text = "Contact Name";
            // 
            // CompanyName
            // 
            this.CompanyName.Location = new System.Drawing.Point(0, 61);
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.Size = new System.Drawing.Size(121, 20);
            this.CompanyName.TabIndex = 4;
            this.CompanyName.Text = "CompanyName";
            // 
            // SupplierId
            // 
            this.SupplierId.Location = new System.Drawing.Point(0, 35);
            this.SupplierId.Name = "SupplierId";
            this.SupplierId.Size = new System.Drawing.Size(121, 20);
            this.SupplierId.TabIndex = 4;
            this.SupplierId.Text = "Id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-3, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Supplier";
            // 
            // ProductPanel
            // 
            this.ProductPanel.Controls.Add(this.IsDiscontinued);
            this.ProductPanel.Controls.Add(this.UnitPriceP);
            this.ProductPanel.Controls.Add(this.Package);
            this.ProductPanel.Controls.Add(this.SupplierIdP);
            this.ProductPanel.Controls.Add(this.ProductName);
            this.ProductPanel.Controls.Add(this.ProductId);
            this.ProductPanel.Controls.Add(this.label3);
            this.ProductPanel.Location = new System.Drawing.Point(12, 39);
            this.ProductPanel.Name = "ProductPanel";
            this.ProductPanel.Size = new System.Drawing.Size(121, 341);
            this.ProductPanel.TabIndex = 6;
            // 
            // IsDiscontinued
            // 
            this.IsDiscontinued.FormattingEnabled = true;
            this.IsDiscontinued.Items.AddRange(new object[] {
            "True",
            "False"});
            this.IsDiscontinued.Location = new System.Drawing.Point(0, 165);
            this.IsDiscontinued.Name = "IsDiscontinued";
            this.IsDiscontinued.Size = new System.Drawing.Size(121, 21);
            this.IsDiscontinued.TabIndex = 14;
            this.IsDiscontinued.Text = "Is Discontinued";
            // 
            // UnitPriceP
            // 
            this.UnitPriceP.Location = new System.Drawing.Point(-1, 113);
            this.UnitPriceP.Name = "UnitPriceP";
            this.UnitPriceP.Size = new System.Drawing.Size(122, 20);
            this.UnitPriceP.TabIndex = 13;
            this.UnitPriceP.Text = "Unit Price";
            // 
            // Package
            // 
            this.Package.Location = new System.Drawing.Point(-1, 139);
            this.Package.Name = "Package";
            this.Package.Size = new System.Drawing.Size(122, 20);
            this.Package.TabIndex = 12;
            this.Package.Text = "Package";
            // 
            // SupplierIdP
            // 
            this.SupplierIdP.Location = new System.Drawing.Point(-1, 87);
            this.SupplierIdP.Name = "SupplierIdP";
            this.SupplierIdP.Size = new System.Drawing.Size(122, 20);
            this.SupplierIdP.TabIndex = 11;
            this.SupplierIdP.Text = "Supplier Id";
            // 
            // ProductName
            // 
            this.ProductName.Location = new System.Drawing.Point(-1, 61);
            this.ProductName.Name = "ProductName";
            this.ProductName.Size = new System.Drawing.Size(122, 20);
            this.ProductName.TabIndex = 10;
            this.ProductName.Text = "Product Name";
            // 
            // ProductId
            // 
            this.ProductId.Location = new System.Drawing.Point(-1, 35);
            this.ProductId.Name = "ProductId";
            this.ProductId.Size = new System.Drawing.Size(122, 20);
            this.ProductId.TabIndex = 9;
            this.ProductId.Text = "Id";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-4, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Product";
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(12, 386);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(119, 23);
            this.Add.TabIndex = 1;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Edit
            // 
            this.Edit.Location = new System.Drawing.Point(12, 415);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(58, 23);
            this.Edit.TabIndex = 1;
            this.Edit.Text = "Edit";
            this.Edit.UseVisualStyleBackColor = true;
            this.Edit.Click += new System.EventHandler(this.Edit_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(76, 415);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(58, 23);
            this.Delete.TabIndex = 1;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // CustomerDataGridView
            // 
            this.CustomerDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CustomerDataGridView.Location = new System.Drawing.Point(140, 39);
            this.CustomerDataGridView.Name = "CustomerDataGridView";
            this.CustomerDataGridView.Size = new System.Drawing.Size(648, 399);
            this.CustomerDataGridView.TabIndex = 6;
            // 
            // OrderDataGridView
            // 
            this.OrderDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderDataGridView.Location = new System.Drawing.Point(140, 39);
            this.OrderDataGridView.Name = "OrderDataGridView";
            this.OrderDataGridView.Size = new System.Drawing.Size(648, 399);
            this.OrderDataGridView.TabIndex = 7;
            // 
            // OrderItemDataGridView
            // 
            this.OrderItemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderItemDataGridView.Location = new System.Drawing.Point(140, 39);
            this.OrderItemDataGridView.Name = "OrderItemDataGridView";
            this.OrderItemDataGridView.Size = new System.Drawing.Size(648, 399);
            this.OrderItemDataGridView.TabIndex = 8;
            // 
            // ProductDataGridView
            // 
            this.ProductDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductDataGridView.Location = new System.Drawing.Point(140, 39);
            this.ProductDataGridView.Name = "ProductDataGridView";
            this.ProductDataGridView.Size = new System.Drawing.Size(648, 399);
            this.ProductDataGridView.TabIndex = 9;
            // 
            // SupplierDataGridView
            // 
            this.SupplierDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupplierDataGridView.Location = new System.Drawing.Point(140, 39);
            this.SupplierDataGridView.Name = "SupplierDataGridView";
            this.SupplierDataGridView.Size = new System.Drawing.Size(649, 399);
            this.SupplierDataGridView.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.SupplierPanel);
            this.Controls.Add(this.SupplierDataGridView);
            this.Controls.Add(this.ProductDataGridView);
            this.Controls.Add(this.ProductPanel);
            this.Controls.Add(this.OrderItemDataGridView);
            this.Controls.Add(this.OrderDataGridView);
            this.Controls.Add(this.CustomerDataGridView);
            this.Controls.Add(this.OrderItemPanel);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.OrderPanel);
            this.Controls.Add(this.Edit);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.SelectTable);
            this.Controls.Add(this.CustomerPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.CustomerPanel.ResumeLayout(false);
            this.CustomerPanel.PerformLayout();
            this.OrderPanel.ResumeLayout(false);
            this.OrderPanel.PerformLayout();
            this.OrderItemPanel.ResumeLayout(false);
            this.OrderItemPanel.PerformLayout();
            this.SupplierPanel.ResumeLayout(false);
            this.SupplierPanel.PerformLayout();
            this.ProductPanel.ResumeLayout(false);
            this.ProductPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderItemDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox SelectTable;
        private System.Windows.Forms.Panel CustomerPanel;
        private System.Windows.Forms.Panel OrderPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Customer;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Edit;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Panel OrderItemPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView CustomerDataGridView;
        private System.Windows.Forms.DataGridView OrderDataGridView;
        private System.Windows.Forms.DataGridView OrderItemDataGridView;
        private System.Windows.Forms.DataGridView ProductDataGridView;
        private System.Windows.Forms.DataGridView SupplierDataGridView;
        private System.Windows.Forms.Panel ProductPanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel SupplierPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SupplierFax;
        private System.Windows.Forms.TextBox SupplierPhone;
        private System.Windows.Forms.TextBox SupplierCountry;
        private System.Windows.Forms.TextBox SupplierCity;
        private System.Windows.Forms.TextBox ContactTitle;
        private System.Windows.Forms.TextBox ContactName;
        private System.Windows.Forms.TextBox CompanyName;
        private System.Windows.Forms.TextBox SupplierId;
        private System.Windows.Forms.TextBox UnitPriceP;
        private System.Windows.Forms.TextBox Package;
        private System.Windows.Forms.TextBox SupplierIdP;
        private System.Windows.Forms.TextBox ProductName;
        private System.Windows.Forms.TextBox ProductId;
        private System.Windows.Forms.ComboBox IsDiscontinued;
        private System.Windows.Forms.TextBox UnitPriceO;
        private System.Windows.Forms.TextBox ProductIdO;
        private System.Windows.Forms.TextBox OrderIdO;
        private System.Windows.Forms.TextBox OrderItemId;
        private System.Windows.Forms.TextBox Quantity;
        private System.Windows.Forms.DateTimePicker OrderDate;
        private System.Windows.Forms.TextBox OrderNumber;
        private System.Windows.Forms.TextBox CustomerIdO;
        private System.Windows.Forms.TextBox OrderId;
        private System.Windows.Forms.TextBox TotalAmount;
        private System.Windows.Forms.TextBox LastName;
        private System.Windows.Forms.TextBox FirstName;
        private System.Windows.Forms.TextBox CustomerId;
        private System.Windows.Forms.TextBox CityC;
        private System.Windows.Forms.TextBox PhoneC;
        private System.Windows.Forms.TextBox CountryC;
    }
}

