﻿//using CodeFirst.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeFirst
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

       
        DataContext dataContext = new DataContext("data source=DESKTOP-L5NMGLP;initial catalog=CodeFirst;integrated security=True;");

        void GetUsers()
        {
            var users = dataContext.Users.ToList();
            dataGridView1.DataSource = users;

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            GetUsers();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            User user = new User()
            {
                FirstName = FirstName.Text.ToString(),
                LastName = LastName.Text.ToString(),
                Email = Email.Text.ToString(),
                Age = int.Parse(Age.Text),
                DateCreated = DateTime.Now,

            };

            dataContext.Users.Add(user);
            dataContext.SaveChanges();
            GetUsers();
        }

        

        private void Update_Click(object sender, EventArgs e)
        {
            User user = new User()
            {
                Id = int.Parse(Id.Text),
                FirstName = FirstName.Text.ToString(),
                LastName = LastName.Text.ToString(),
                Email = Email.Text.ToString(),
                Age = int.Parse(Age.Text),
                DateCreated = DateTime.Now,

            };

            dataContext.Users.AddOrUpdate(user);
            dataContext.SaveChanges();
            GetUsers();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            int ID = int.Parse(Id.Text);
            User user =  dataContext.Users.FirstOrDefault(x => x.Id == ID);


 
            dataContext.Users.Remove(user);
            dataContext.SaveChanges();
            GetUsers();
        }
    } 
}

